# Monopoly Game

## Team information
Team ID - 8  

Brandon Blacoe - 14344171

Louis Gunning - 15310591

## Repository Structure - Packages
<p> The repository is structured by package, with one package for the Game source code and another for the JUnit test cases.
Each package is broken down into smaller subpackages which are grouped based on functionality. The structure of how classes
interact is shown in the UML diagrams folder, and will also be discussed in the Design section of the ReadMe.</p>

### cards
<p> The cards package contains all of the classes relating to the Chance and Community Chest Decks. Each class describes
some functionality that cards can be grouped into. For example, cards that require the player to move to a specific location
are all instances of the same classes with different parameters that determine the location. </p>

### game
<p> This package contains the classes that form the core of the game: the Player, Box, Deck, Dice, Board and Token.
The main function is also contained within this package in the "Game" class. </p>

### menus
<p> The menus are all contained within this package. If an action performed between the user and a particular tile or card
requires user interaction, a menu within this package is accessed and displayed to the console. Keeping the basic i/o separate 
from the other packages makes it easier to handle input exceptions. </p>

### setup
<p> The setup package contains the classes and methods that 'build' the game before it is launched. The information that dictates
the functionality and content of cards and tiles in the game is read from text files using these methods. The methods then return
the game objects after interpreting them from the text.</p>

### tiles
<p> The tiles are the individual spaces on the board that players can land on. These tiles are loaded from the setup package
and converted into game objects. The tiles are grouped based on their functionality, so property tiles form their own hierarchy,
as do the 'passive' tiles that do not require user input. </p>

## How To Run
<p> The game is run from a JAR file located in the 'Executable' folder in the repository.  You will
 need to download the entire folder as it contains
 text files necessary for setup. Once you download the folder,
 just run the executable JAR file from inside the folder on your computer. To play the game you just need a keyboard, and at certain points
the game will prompt input from you to continue. In these cases, the options you have will appear in a menu (generally you will
have to choose a number or letter) and at that point you just enter your choice followed by the ENTER key.</p>

## Rules of the Game

### Setup
<p> The game begins by prompting the user to input the number of players that are playing, you can choose within the range 1-6. 
Next, for each player you will have to enter a name and choose a token (note, you can customise the tokens by editing the
text file in the repository). Finally you will have to choose the amount of starting money that each player should have.</p>

### Each Turn
<p> The game will prompt each player to take their turn, and at the start of each turn a start menu will appear. This menu is a portal
to the various actions that the player can take. 

If the player chooses (2), then a list of their properties will appear. This menu is dynamic,
and will update as you buy new properties, but it will also change as the game goes on. If you are able to develop a property, a '+' will appear
next to it. By selecting a property you can view the title deeds card. This menu is also dynamic, so if you develop this property the rent
will change to reflect that. In this menu a number of options can appear: buy/sell house, buy/sell hotel, and sell. These options appear
only when you are able to perform the action associated with them, so if you can't afford to buy a house, the option to do it won't appear.

If the player chooses (3), they can attempt to sell a get out of jail card to another player. This opens a bargaining interface where
the player can haggle with their buyer.

If the player chooses (1), the turn will commence with the dice roll. The player will land on a tile, and the associated action will be 
performed. </p>

### Doubles
<p> The start menu indicates which roll a player is on. If a player rolls doubles, they can continue to a second turn. If a player rolls
three doubles in a row, their turn ends and they are sent to Jail. </p>

### GO
<p> Passing GO awards the player $200. No extra reward is given for landing on the GO tile. Certain cards allow the player to pass GO, but
without receivng their $200. </p>

### Landing on Property
<p> If an unowned property is landed on, the player will be prompted for an action. If they choose to buy, it will be added to their inventory.
If the player declines, or if they cannot afford the property it goes to auction.

If an owned property is landed on, the player will be prompted to pay the owner. If they can't afford it, they will immediately go bankrupt and
forfeit all their belongings, which are transferred to the owner. </p>

### Special Rents
<p> Utilites and Railroads both have variable rents depending on the number of each owned. The player can view this information by navigating
to the title deeds menu for that property, where the rent will be displayed. The same is true for developed streets, and streets that are 
undeveloped but part of a colour group that is fully owned. </p>

### Jail
<p> Get out of jail cards can be acquired from the Chance or Community Chest Decks. These cards can be retained until needed, or sold from the
menu at the start of each turn. If another player is in jail, you may be asked by them to sell your card to free them. In this case, a menu
will appear with the prompt, where you can name your price or decline the offer. 

If you draw a jail card, land on the Go To Jail tile, or roll three doubles you will be in jail and your turn will end. 
Once in jail, you can be offered up to 4 options on how to proceed. You will always be given the option to attempt to roll for doubles,
 and if you do you will be released. If you can afford the fine of $50, then the option will appear and you can pay it. If you have 
 a get out of jail card, then this option will also appear. If another player has a get out of jail card, then an option will
 appear for you to buy it from them.</p>
 
### Building Developments
 <p> Developments can be built via the title deeds menu for each property. Houses and hotels can only be built if the entire colour group on which
 they will be built is owned by the player. They must also be built linearly across each property in a colour group. If the houses in the box
 run out, then a building shortage goes into affect, and no houses or hotels can be bought. Provided that these conditions are met in the player's
 favour, the option to build will appear in the menu. If the player cannot afford to buy a house or hotel, or if for some other reason they
 cannot be built, the option will simply not appear in the menu and the player cannot purchase. Properties that can be developed are indicated with
 a '+' in the property list. </p>
 
### Selling Property
 <p> Property can be sold at any time through the player menu. This menu gives the player the option to choose another player to sell to, and then 
 to name their price. If the second player accepts, the deal is done and the properties exchanged. Otherwise, the first player is prompted again
 to name a different price. This process will continue until either the property is sold, or the selling player resigns their effort by entering a
 (-1) in the console.
 
 If a property has developments on it, it cannot be sold, and the title deeds card menu will not display the option until all developments
 are removed.</p>
 
### Auctions
 <p> Auctions have a special menu which is triggered when either the player doesn't buy a property when landing on it, or when a player loses
 their properties to the bank in bankruptcy. An auction gives each player the option to bid, including the player who originally passed on 
 purchasing the property. Each player will be prompted with a menu stating the highest bid and requesting them to input whether or not
 they wish to bid. If a player chooses to bid, a new menu appears which asks them to input their bid. This input is upper bounded by the amount that
 the player has in their account, and lower bounded by the current highest bid +$1. If a player cannot afford to bid, then they are skipped.
 When only one bidder remains, the property goes to them. If all players decline, then the property goes to the player who landed on the property
 for whatever amount they choose. </p>
 
### Bankruptcy
 <p> Bankruptcy falls into two categories: to bank and to player. To player bankruptcy cannot be recovered from, the player immediately must
 turn over all belongings to the player they are indebted to.
 To bank bankruptcy opens the bankruptcy menu. From here, you can sell any houses/hotels or properties you have remaining. The interface for 
 this is the same as selling properties from the player menu, except that the option to sell properties to any players is removed, and instead
 you can only sell to the bank. If you manage to recuperate your losses here, the game will continue.
 
### Winning
 <p> The game will end when two players go bankrupt, at which point the value of each player will be calculated based on their belongings,
 and a ranked list will be produced of all players in the game. </p>

## Team Work Organisation
<p> The work was roughly divided on a per-class basis, with Louis primarily focusing on tiles, properties and cards, and Brandon creating the
menus, setup and main classes. We both created unit tests as we went, and at the end performed unit testing and user testing as a final check
on the code. The work was divided 50/50, and we both ended up editing every class at some point. </p>

## Design
<p> To implement the Monopoly game into code, we took advantage of a number of design approaches.</p>

### Singleton
<p> The dice and box classes were both implemented with the Singleton approach. The dice class represents the two dice in the game, and thus there 
should only be one instance of it. Likewise, the box contains the houses and hotels, and there should only be one instance of this as well.
The getInstance() method retrieves the single instance of each class, and if none exists it creates a new one.</p>

### Interfaces
<p> The most important interface in the game is the 'Interactable', which defines the criteria for objects that players can interact with.
Every tile and every card implements this interface, while allows us to abstract the details of how each component works. We also created
a 'Passive' interface, for objects that only display a message when players interact with them, and don't actually change the game state
in any way. The Comparable interface was essential for ranking the players based on their scores, and also for ordering the tiles when
the board is initialised.</p>

### Façade and Overrides
<p> Cards and Tiles both implement the 'Interactable' interface, but each tile and card has a unique functionality. To simplify the interpretation
of the system, we created a generic superclass for Tile and Card. This superclass has generic methods that apply to all subclasses, which each subclass
has to override with their own unique functionality. Ultimately, when the game is implemented in the main function, the complexities of the different cards and tiles is hidden
by the tile.interact() method.</p>

### Managing User Input
<p> The game is centred around user input, but input streams make unit testing more challenging, and they can also introduce more errors.
To circumvent this issue, we created a package called "menus" which handles all i/o. Within menus we have the class InputHandler, which
is the only class that accepts input from the user. This class accepts three forms of input within three different functions, one which
asks for the user to hit Enter, one which asks for an integer input, and one which asks for string input.</p>

#### Prompt Enter
<p> The prompt enter method asks the user to hit enter, and is protected by a try-catch.</p>

#### Input Integer
<p> This method asks the user to input an integer within a given range, is protected by a try-catch, and also iterates until the user
inputs an accepted integer. Once the integer is accepted, the method returns this integer to whichever method needs it.</p>

#### Input String
<p> This method asks the user to input a string from a list of options, is also protected by the try-catch, and iterates until the user 
succesfully inputs a string that is acceptable. The accepted string is returned to whichever method needs it. </p>

### Menus
<p> The premise of the menus is to separate any code that supports the UI in the console, and functional 'back end' code. The menus are separated
based on their function, with most of them returning some variable that is used by another function. For example, when a player lands on a 
property, the property-action menu is called, which prompts the user to choose to buy or auction. This menu returns a boolean to the 
property tile, which handles the rest of the operation.</p>

#### Dynamic Menus
<p> Menus hide information that is not yet relevant to the user. This helps cut down on code that handles edge-cases. The title deeds menu
will only display the option to purchase houses and hotels once all of the criteria are met. Likewise, the jail menu only offers the 
player options to get out of jail that they can attain: e.g. if they can't pay the fine then this option does not appear.</p>