package tiletests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import game.Player;
import game.Token;
import tiles.Property;
import tiles.Railroad;

class TestRailroad {

	@Test
	void testRailroad() {
		Property ReaRa = new Railroad("Reading Railroad", 0, 100, 25);
		assertEquals(ReaRa.getName(), "Reading Railroad");
		assertEquals(ReaRa.getPosition(), 0);
		assertEquals(ReaRa.getPrice(), 100);
	}
	
	@Test
	void testBuyAndRentRailroad() {
		Token token = new Token("Red", "Car"); 
		Property ReaRa = new Railroad("Reading Railroad", 0, 100, 25);
		Property washRa = new Railroad("Washington Railroad", 0, 100, 25);
		Player player1 = new Player("Jim", token, 0);
		Player player2 = new Player("Jane", token, 0);
		ArrayList<Player> players = new ArrayList<Player>();
		
		player1.deposit(200);
		player2.deposit(100);
		ReaRa.interact(player1, players, null, null, null);
		washRa.interact(player1, players, null, null, null);
		assertEquals(player1.getAccount(), 0);
		ReaRa.interact(player2, players, null, null, null);
		assertEquals(50, player1.getAccount());
		assertEquals(50, player2.getAccount());
		assertEquals(player1, ReaRa.getOwner());
	}

}
