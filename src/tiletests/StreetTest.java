package tiletests;
import game.*;
import tiles.Property;
import tiles.Street;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StreetTest {

	@Test
	void testHousesAndHotels() {
		Token token = new Token("Red", "Car");
		Box box = Box.getInstance(null, null);
		int[] rentHouses = {200, 300, 400, 500, 1000};
		Property washSt = new Street("Washington Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Property brianSt = new Street("Brian Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Property broadSt = new Street("Broad Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Player player1 = new Player("Jim", token, 0);
		player1.deposit(2000);
		player1.addProperty(washSt);
		player1.addProperty(brianSt);
		
		assertFalse(player1.buyHouse((Street)washSt, box));
		
		player1.addProperty(broadSt);
		assertEquals(player1.getListOfProperties(), "Washington Street, Brian Street, Broad Street");
		
		assertTrue(player1.buyHouse((Street)washSt, box));
		assertFalse(player1.buyHouse((Street)washSt, box));
		assertFalse(player1.buyHotel((Street)washSt, box));
		
		assertTrue(player1.buyHouse((Street)brianSt, box));
		assertFalse(player1.buyHouse((Street)brianSt, box));
		
		assertTrue(player1.buyHouse((Street)broadSt, box));
		assertTrue(player1.buyHouse((Street)broadSt, box));
		assertFalse(player1.buyHouse((Street)broadSt, box));
		
		assertTrue(player1.buyHouse((Street)washSt, box));
		assertTrue(player1.buyHouse((Street)brianSt, box));
		
		assertTrue(player1.buyHouse((Street)washSt, box));
		assertTrue(player1.buyHouse((Street)brianSt, box));
		assertTrue(player1.buyHouse((Street)broadSt, box));
	}

}
