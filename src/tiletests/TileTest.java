package tiletests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import tiles.Tile;

class TileTest {

	@Test
	void testTile() {
		Tile newTile = new Tile("Washington Street", 16);
		assertEquals("Washington Street", newTile.getName(), "Name wrong");
		assertEquals(16, newTile.getPosition(), "Position wrong");
	}

}
