package tiletests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import tiles.GoToJail;

class GoToJailTest {

	@Test
	void testGoToJail() {
		GoToJail gTJ = new GoToJail("GO TO JAIL", 0);
		assertTrue(gTJ.getName().equals("GO TO JAIL"));
	}

}
