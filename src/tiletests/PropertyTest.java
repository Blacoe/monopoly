package tiletests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import game.Player;
import tiles.Property;

class PropertyTest {

	@Test
	void testProperty() {
		Property p = new Property("Property", 0, 100, 30);
		assertFalse(p.getIsOwned());
		Player p1 = new Player("Jim", null, 100);
		p1.addProperty(p);
		assertTrue(p.getIsOwned());
	}

}
