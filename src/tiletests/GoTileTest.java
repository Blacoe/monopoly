package tiletests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import tiles.GoTile;

class GoTileTest {

	@Test
	void testGoTile() {
		GoTile gt = new GoTile("GO", 0);
		assertTrue(gt.getName().equals("GO"));
	}

}
