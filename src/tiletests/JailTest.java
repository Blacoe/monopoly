package tiletests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import tiles.Jail;

class JailTest {

	@Test
	void testJail() {
		Jail j = new Jail("Jail", 0);
		assertTrue(j.getName().equals("Jail"));
	}

}
