package tiletests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import tiles.CardTile;

class CardTileTest {

	@Test
	void testCardTile() {
		CardTile ct = new CardTile("This is a tile", 0, true);
		assertTrue(ct.getName().equals("This is a tile"));
	}

}
