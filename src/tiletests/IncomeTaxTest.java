package tiletests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import tiles.IncomeTax;

class IncomeTaxTest {

	@Test
	void testIncomeTax() {
		IncomeTax iT = new IncomeTax("Income Tax", 0, 100);
		assertEquals(0, iT.getPosition());
		assertTrue(iT.getName().equals("Income Tax"));
	}

}
