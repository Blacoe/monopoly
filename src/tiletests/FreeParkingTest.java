package tiletests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import tiles.FreeParking;
import tiles.Tile;

class FreeParkingTest {

	@Test
	void testFreeParking() {
		FreeParking fp = new FreeParking("Free Parking!", 0);
		assertTrue(fp instanceof Tile);
		assertTrue(fp.getName().equals("Free Parking!"));
	}

}
