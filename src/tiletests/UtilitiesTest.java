package tiletests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import game.Dice;
import game.Player;
import tiles.Utilities;

class UtilitiesTest {

	@Test
	void testCalculateRent() {
		Player player1 = new Player("Jim", null, 10000);
		Dice dice = Dice.getInstance();
		Utilities elec = new Utilities("electric", 100, 100, 100);
		Utilities water = new Utilities("water", 100, 100, 100);
		player1.addProperty(elec);
		dice.roll();
		int roll = dice.getDiceRoll();
		int rent = elec.calculateRent(dice);
		assertEquals(roll*4, rent);
		player1.addProperty(water);
		dice.roll();
		roll = dice.getDiceRoll();
		rent = elec.calculateRent(dice);
		assertEquals(roll*10, rent);
	}

}
