package cardtests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import cards.MovePlacesCard;
import game.Box;
import game.Deck;
import game.Dice;
import game.Gameboard;
import game.Player;
import setup.DeckCreator;
import setup.GameboardCreator;

class MovePlacesCardTest {

	@Test
	void test() {
		Dice pairOfDice = Dice.getInstance();
		Deck communityChestDeck = DeckCreator.createDeck("OfficialCommunityChest.txt");
		Deck chanceDeck = DeckCreator.createDeck("OfficialChance.txt");
		Box box = Box.getInstance(communityChestDeck, chanceDeck);
		String filename = "CustomGameboard.txt";
		Gameboard gb = GameboardCreator.createGameboard(filename);
		Player p1 = new Player("John", null, 0);
		ArrayList<Player> players = new ArrayList<Player>();  
		players.add(p1);
		MovePlacesCard mpc = new MovePlacesCard("Move 3 places", 20);
		mpc.interact(players.get(0), players, box, gb, pairOfDice);
		assertEquals(20, players.get(0).getPlayerPosition(), "Player did not move to the correct position");
	}

}
