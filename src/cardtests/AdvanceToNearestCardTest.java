package cardtests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import cards.AdvanceToNearestCard;
import game.Box;
import game.Dice;
import game.Gameboard;
import game.Player;
import setup.GameboardCreator;

class AdvanceToNearestCardTest {

	// Test for utility card
	
	@Test
	void testAdvanceToNearestCard() {
		Dice pairOfDice = Dice.getInstance();
		Box box = Box.getInstance(null, null);
		String filename = "CustomGameboard.txt";
		Gameboard gb = GameboardCreator.createGameboard(filename);
		Player p1 = new Player("John", null, 0);
		ArrayList<Player> players = new ArrayList<Player>();  // Create an array list to hold the player instance
		players.add(p1);
		AdvanceToNearestCard atnc = new AdvanceToNearestCard("Utility mover", "Utilities");
		atnc.interact(p1, players, box, gb, pairOfDice);
		assertEquals(12, players.get(0).getPlayerPosition(), "Incorrect player position");
	}
	
	// Test for rail card
	
	@Test
	void testAdvanceToNearestCardRailroad() {
		Dice pairOfDice = Dice.getInstance();
		Box box = Box.getInstance(null, null);
		String filename = "CustomGameboard.txt";
		Gameboard gb = GameboardCreator.createGameboard(filename);
		Player p1 = new Player("John", null, 0);
		ArrayList<Player> players = new ArrayList<Player>();  // Create an array list to hold the player instance
		players.add(p1);
		AdvanceToNearestCard atnc = new AdvanceToNearestCard("Incorrect player position", "Railroad");
		atnc.interact(p1, players, box, gb, pairOfDice);
		assertEquals(5, players.get(0).getPlayerPosition(), "Text is not as expected");
	}
}
