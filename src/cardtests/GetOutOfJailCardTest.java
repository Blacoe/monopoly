package cardtests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import cards.GetOutOfJailCard;
import game.Player;

class GetOutOfJailCardTest {

	@Test
	void test() {
		GetOutOfJailCard outOfJail = new GetOutOfJailCard("Out of jail");
		Player p1 = new Player("John", null, 100);
		ArrayList<Player> players = new ArrayList<Player>();  // Create an array list to hold the player instance
		players.add(p1);
		outOfJail.interact(p1, players, null, null, null);
		assertEquals(1, players.get(0).getNumGetOutOfJailCards(), "Player did not receive get out of jail card");
	}

}
