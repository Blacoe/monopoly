package cardtests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import cards.*;
import game.*;

class CollectMoneyCardTest {

	@Test
	void testCollectMoneyCard() {
		Box box = Box.getInstance(null, null);
		CollectMoneyCard cmc = new CollectMoneyCard("Take 100 dollars", 100);
		Player p1 = new Player("John", null, 100);
		ArrayList<Player> players = new ArrayList<Player>();  // Create an array list to hold the player instance
		players.add(p1);
		assertEquals(p1.getAccount(), 100);
		cmc.interact(p1, players, box, null, null);
		assertEquals(p1.getAccount(), 200);
	}

}
