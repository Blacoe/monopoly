package cardtests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cards.Card;

class CardTest {

	@Test
	void testCard() {
		Card singleCard = new Card("Go to Go and collect 200");
		assertEquals("Go to Go and collect 200", singleCard.getText(), "Text is not as expected");
	}

}
