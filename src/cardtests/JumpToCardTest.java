package cardtests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import cards.JumpToCard;
import game.Box;
import game.Dice;
import game.Gameboard;
import game.Player;
import setup.GameboardCreator;

class JumpToCardTest {

	@Test
	void test() {
		Dice pairOfDice = Dice.getInstance();
		Box box = Box.getInstance(null, null);
		String filename = "CustomGameboard.txt";
		Gameboard gb = GameboardCreator.createGameboard(filename);
		Player p1 = new Player("John", null, 0);
		p1.movePlayer(15, gb.getTileSet().size(), box);
		ArrayList<Player> players = new ArrayList<Player>();  
		players.add(p1);
		JumpToCard jtc = new JumpToCard("Jump to 10 without passing Go", 10);
		jtc.interact(p1, players, box, gb, pairOfDice);
		assertEquals(10, players.get(0).getPlayerPosition(), "Player did not move to the correct position");
		assertEquals(0, players.get(0).getAccount(), "Player earned money passing Go");
	}

}
