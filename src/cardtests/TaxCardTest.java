package cardtests;

import static org.junit.jupiter.api.Assertions.*;

import cards.*;
import game.*;

import org.junit.jupiter.api.Test;

class TaxCardTest {

	@Test
	void testTaxCard() {
		Box box = Box.getInstance(null, null);
		TaxCard tC = new TaxCard("Take 100 dollars", 100);
		Player p1 = new Player("John", null, 100);
		assertEquals(p1.getAccount(), 100);
		tC.interact(p1, null, box, null, null);
		assertEquals(p1.getAccount(), 0);
	}

}
