package cardtests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import cards.PayEachPlayerCard;
import game.Box;
import game.Player;

class PayEachPlayerCardTest {

	@Test
	void test() {
		PayEachPlayerCard pepc = new PayEachPlayerCard("Take my money!",200);
		Box box = Box.getInstance(null, null);
		Player p1 = new Player("John", null, 1000);
		Player p2 = new Player("Mary", null, 1000);
		Player p3 = new Player("Frank", null, 1000);
		ArrayList<Player> players = new ArrayList<Player>();  // Create an array list to hold the player instance
		players.add(p1);
		players.add(p2);
		players.add(p3);
		pepc.interact(p1, players, box, null, null);
		assertEquals(600, players.get(0).getAccount(), "Text is not as expected");
		assertEquals(1200, players.get(1).getAccount(), "Text is not as expected");
		assertEquals(1200, players.get(2).getAccount(), "Text is not as expected");
	}
}
