package cardtests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import cards.HouseFeeCard;
import game.Box;
import game.Player;
import tiles.Property;
import tiles.Street;

class HouseFeeCardTest {

	@Test
	void test() {
		HouseFeeCard hfc = new HouseFeeCard("This card takes money for houses and hotels", 10, 40);
		Box box = Box.getInstance(null, null);
		Player p1 = new Player("John", null, 1000);
		ArrayList<Player> players = new ArrayList<Player>();  // Create an array list to hold the player instance
		players.add(p1);
		int[] rentHouses = {200, 300, 400, 500, 1000};
		Property washSt = new Street("Washington Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Property brianSt = new Street("Brian Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Property broadSt = new Street("Broad Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		p1.addProperty(washSt);
		p1.addProperty(brianSt);
		p1.addProperty(broadSt);
		((Street) p1.getProperties().get(0)).buildHouse(p1);
		((Street) p1.getProperties().get(1)).buildHouse(p1);
		((Street) p1.getProperties().get(1)).buildHouse(p1);
		((Street) p1.getProperties().get(2)).buildHouse(p1);
		((Street) p1.getProperties().get(2)).buildHouse(p1);
		((Street) p1.getProperties().get(0)).buildHotel(p1);
		((Street) p1.getProperties().get(2)).buildHotel(p1);
		hfc.interact(p1, players, box, null, null);
		assertEquals(900, players.get(0).getAccount(), "Text is not as expected");
	}
}
