package menus;

import java.util.ArrayList;

import game.Box;
import game.Player;

public class BankruptcyMenus {
	static public void bankruptcySellingWindow(Player p, Box box, ArrayList<Player> players) {
		// this menu gives the player options to sell their items
		String[] lines = new String[2];
		lines[0] = "(Enter 1) Sell Properties or Developments";
		lines[1] = "(Enter 2) Sell Get Out of Jail Card";
		
		MenuGeneration.printMenu(MenuGeneration.makeWindow(p.getName() +  " BANKRUPT", lines, MenuGeneration.sizeOfLine));
		
		int choice = InputHandler.integerInputValidation(1, 2);
		//if they choose to sell a property, go to the menu
		if(choice == 1) {
			PropertyMenus.propertyMenu(p, 0, box, 1, players);
		}
		//if they choose to sell a card, go to the menu
		else if(choice == 2 && p.getNumGetOutOfJailCards() > 0) {
			JailCardTransactions.sellJailCardActionMenu(p, 0, box, players, 1);
		}
		else {
			System.out.println("You dont' have any!");
		}
	}
	
	//while the player is bankrupt, they can choose to sell items
	static public boolean bankruptcyActionMenu(Player p, Box box, int debt, ArrayList<Player> players) {
		while(p.getProperties().size() > 0 || p.getNumGetOutOfJailCards() > 0) {
			//if after selling something, the player can now afford their debt
			//the method returns true and the recovery message is shown
			if(p.canAfford(debt)) {
				System.out.println(p.getName() + ", you can afford to pay off your debt now");
				return true;
			}
			//as long as they have properties to sell this loop repeats, opening
			//the selling window
			bankruptcySellingWindow(p, box, players);
		}
		return false;
	}
	
	//if the player can recover their losses after being bankrupt to a bank
	//then this message congratulates them
	public static void backFromBankruptcy(Player p) {
		String[] backMessage = new String[2];
		backMessage[0] = "You have recovered from";
		backMessage[1] = "bankrupcty, " + p.getName();
		MenuGeneration.printMenu(MenuGeneration.makeWindow("GOOD GRACES!", backMessage, MenuGeneration.sizeOfLine));
	}
	
	//if the player is bankrupt to another player, the game ends for them
	//a sad message is displayed
	public static void bankruptcyToOtherPlayerMenu(Player p, Player p2) {
		String[] loseMessage = new String[3];
		loseMessage[0] = "Sorry, but it looks like you lost, " + p.getName();
		loseMessage[1] = "";
		loseMessage[2] = "Your belongings are being transferred to " + p2.getName();
		MenuGeneration.printMenu(MenuGeneration.makeWindow("YOU LOSE!", loseMessage, MenuGeneration.sizeOfLine));
	}
	
	public void endTurn() {
		System.out.print("Turn Over\n");
	}

}
