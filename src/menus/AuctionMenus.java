package menus;

import java.util.ArrayList;

import game.Box;
import game.Player;
import tiles.Property;

public class AuctionMenus {
	
	//auction menu gets triggered when player lands on unwanted property or,
	//when player goes bankrupt to bank
	public static void auctionMenu(ArrayList<Player> players, Property p, Box box) {
		ArrayList<Player> bidders = new ArrayList<>(players);
		Player highestBidder = players.get(0);
		boolean saleAgreed = false; //will be set to true when an agreement is reached
		int highestBid = 0;
		String[] lines = {"Auction For " + p.getName()};
		MenuGeneration.printMenu(MenuGeneration.makeWindow("PROPERTY AUCTION", lines, MenuGeneration.sizeOfLine));
		InputHandler.promptEnterKey();
		while(!saleAgreed) {
			//loop through all the bidders
			for(int i = 0; i<bidders.size(); i++) {
				if(!bidders.get(i).equals(highestBidder)) {
					//for each bidder, open the enterBid menu to let them bid
					int newHighestBid = enterBid(bidders, i, highestBid, p);
					//keep track of highest bid and bidder
					if(newHighestBid > highestBid) {
						highestBidder = players.get(i);
						highestBid = newHighestBid;
					}
				}
				//if only one bidder remains, sale is agreed and property is sold
				if(bidders.size() == 1) {
					saleAgreed = true;
					String[] list = {p.getName() + " SOLD!", "For $" + highestBid, "To " + bidders.get(0).getName() };
					MenuGeneration.printMenu(MenuGeneration.makeWindow("PROPERTY AUCTION", list, MenuGeneration.sizeOfLine));
					highestBidder.withdraw(highestBid);
					highestBidder.addProperty(p);
					InputHandler.promptEnterKey();
				}
			}
		}
	}
	
	//menu triggered by previous one
	public static int enterBid(ArrayList<Player> bidders, int i, int highestBid, Property prop) {
		Player p = bidders.get(i);
		int bid = 0;
		//if player can't afford, take them out of the bidding list
		//return same highest bid as before
		if(p.getAccount() <= highestBid) {
			bidders.remove(i);
			return highestBid;
		}
		String[] list = new String[6];
		list[0] = "Bid for " + prop.getName();
		list[1] = "Highest Bid: " + highestBid;
		list[2] = "Do you want to bid?";
		list[3] = "";
		list[4] = "(Enter 1) Yes";
		list[5] = "(Enter 2) No";
		MenuGeneration.printMenu(MenuGeneration.makeWindow(p.getName().toUpperCase() + "'S TURN TO BID", list, MenuGeneration.sizeOfLine));
		int choice = InputHandler.integerInputValidation(1, 2);
		//if player chooses to bid, they must input a bid
		//bid is updated to the new highest bid
		if(choice == 1) {
			System.out.println("Enter your bid:");
			bid = InputHandler.integerInputValidation(highestBid+1, p.getAccount());
		}
		//if player chooses no, remove from bidding list
		if(choice == 2) {
			bid = highestBid;
			bidders.remove(i);
		}
		return bid;
	}
	
}
