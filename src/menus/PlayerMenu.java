package menus;
import java.util.ArrayList;
import game.Box;
import game.Player;
public class PlayerMenu {

	// This is the class that is executed at the start of every players turn. It shows the options available to the player by using logic.
	// This class prints out the menu for the player in different layers, it takes inputs for each layer and perform the associated action
	// It takes the player input, depending on the input a new menu will be called.
	
	public static void startTurnMenu(Player p, int rolls, Box box, ArrayList<Player> players) {
		int choice = 0;
		while(choice!=1) {
			// print the main menu
			String[] lines = new String[9];
			lines[0] = "Token: "+  p.getToken().getColour() + " " + p.getToken().getName();
			lines[1] = "Board position: " + p.getPlayerPosition();
			lines[2] = "Money: " + p.getAccount();
			lines[3] = "Get out of Jail Cards: " + p.getNumGetOutOfJailCards();
			if(rolls==0) {
				  lines[4] = "Status: First dice roll!";
			}
			else if(rolls ==1) {
				lines[4] = "Status: Second dice roll!";
			}
			else if(rolls ==2) {
				lines[4] = "Status: Third dice roll!";
			}	
			lines[5] = "";
			lines[6] = "(Enter 1) Roll Dice";
			lines[7] = "(Enter 2) View/Interact with Properties";
			lines[8] = "(Enter 3) Sell Get Out of Jail Card";
			
			MenuGeneration.printMenu(MenuGeneration.makeWindow(p.getName().toUpperCase() + "'S TURN", lines, MenuGeneration.sizeOfLine));
			
			choice = InputHandler.integerInputValidation(1, 3);
			if(choice == 2) {
				PropertyMenus.propertyMenu(p, rolls, box, 0, players);
			}
			if(choice == 3) {
				if(p.getNumGetOutOfJailCards() > 0) {
					JailCardTransactions.sellJailCardActionMenu(p, rolls, box, players, 0);
				}
				else {
					String[] noCardMessage = {"You don't have a Jail-Break Card", "", "YET!","(Enter 1) Go Back"};
					MenuGeneration.printMenu(MenuGeneration.makeWindow(p.getName().toUpperCase() + "'S CARDS", noCardMessage, MenuGeneration.sizeOfLine));
					InputHandler.integerInputValidation(1,1);
				}
			}
		}
	}
	

	

}