package menus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class InputHandler {
	
	//some actions require the user to hit ENTER
	public static void promptEnterKey(){
		System.out.println("Press \"ENTER\" to continue...\n");
		try {
			System.in.read();
			System.in.skip(System.in.available());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	//some actions require user to choose one option from many
	//the various options are passed as an array list
	//the user selects one of these options
	//this option is returned as a string
	@SuppressWarnings("resource")
	//system.in can't be closed so suppress warning
	public static String stringOptionsInputValidation(ArrayList<String> options){ 
		String string;
			Scanner sc = new Scanner(System.in);
			do {
			    System.out.println("Please enter your option");
			    while (!sc.hasNext()) {
			        System.out.println("That's not a String!");
			        sc.next(); 
			    }
			    string = sc.next();
			} while (!options.contains(string));
			//the loop will keep iterating until the user inputs the a valid option
			System.out.println("You chose: " + string);
		return string;
	}
	
	//some actions require user input as an integer
	//the lowest acceptable integer and highest are passed as parameters
	//the chosen action is returned as an integer
	@SuppressWarnings("resource")
	//system.in can't be closed so suppress warning
	public static int integerInputValidation(int optionRangeLower, int optionRangeUpper){ 
		int number;
			Scanner sc = new Scanner(System.in);
			do {
			    System.out.println("Please choose an action within the range of options ["+optionRangeLower+" to "+optionRangeUpper+"]: ");
			    while (!sc.hasNextInt()) {
			        System.out.println("That's not a number!");
			        sc.next(); 
			    }
			    number = sc.nextInt();
			} while (number < optionRangeLower || number > optionRangeUpper);
			//the loop will keep iterating until the user inputs the a valid option
			System.out.println("You chose: " + number);
		return number;
		}
	
	
}
