package menus;

import java.util.ArrayList;

import game.Box;
import game.Player;

public class JailMenus {
	
	//if player is in jail they need to choose what to do
	//their list of choices:
	//1. roll doubles, 2. pay the fine, 3. use card, 4. buy card
	//these options are dynamically added to the menu depending
	//on their availability
	public static int inJailMenu(Player p, ArrayList<Player> players, Box box) {
		int choiceInt = 5;
		while(choiceInt == 5) {
			int n = 0;
			ArrayList<String> options = new ArrayList<>();
			ArrayList<String> optionLines = new ArrayList<>();
			options.add("" + 1);
			options.add("" + 2);
			//if they can afford the fine, add the option
			if(p.canAfford(50)) {
				n = n + 1;
				options.add("" + 2);
				optionLines.add("(Enter 2) Pay the Fine: $50");
			}
			//if they have a card, add the option
			if(p.getNumGetOutOfJailCards() > 0) {
				n = n + 1;
				options.add("" + 3);
				optionLines.add("(Enter 3) Use your Jail-Break Card");
			}
			//if they can buy a card, add the option
			for(int x = 0; x < players.size(); x++) {
				if(!(players.get(x).equals(p))) {
					if(players.get(x).getNumGetOutOfJailCards() > 0) {
						n = n + 1;
						options.add("" + 4);
						optionLines.add("(Enter 4) Buy a Jail-Break Card");
					}
				}
			}
			String[] jailStrings = new String[15+n];
			jailStrings[0] = "||   ||     ||   ||";
			jailStrings[1] = "||   ||, , ,||   ||";
			jailStrings[2] = "||  (||/|/(\\||/  ||";
			jailStrings[3] = "||  ||| _'_`|||  ||";
			jailStrings[4] = "||   || o o ||   ||";
			jailStrings[5] = "||  (||  - `||)  ||";
			jailStrings[6] = "||   ||  =  ||   ||";
			jailStrings[7] = "||   ||\\___/||   ||";
			jailStrings[8] = "||___||) , (||___||";
			jailStrings[9] = "/||---||-\\_/-||---||\\";
			jailStrings[10] = "/ ||--_||_____||_--|| \\";
			jailStrings[11] = "(_(||)-| S123-45 |-(||)_)";
			jailStrings[12] = " ";
			jailStrings[13] = "(Enter 1) Roll doubles to get out.";
			for(int j = 0; j < n; j++) {
				jailStrings[14+j] = optionLines.get(j);
			}
			jailStrings[14+n] = "(Enter 5) View/Interact with Properties";
			options.add("" + 5);
			String[] jailWindow = MenuGeneration.makeWindow(p.getName()+" is in Jail!", jailStrings , MenuGeneration.sizeOfLine);
			MenuGeneration.printMenu(jailWindow);
			String choice = InputHandler.stringOptionsInputValidation(options);
			choiceInt = Integer.parseInt(choice);
			//because of input validation, the choice will be 1-4
			if(choiceInt == 5) {
				PropertyMenus.propertyMenu(p, 1, box, 0, players);
			}
		}
		return choiceInt;
	}
	

	//the player is trying to buy a get out of jail card from someone
	public static boolean seekJailCard(Player buyer, ArrayList<Player> players) {
		ArrayList<Player> cardHolders = new ArrayList<Player>();
		for(int i = 0; i < players.size(); i++) {
			if(players.get(i).getNumGetOutOfJailCards() > 0) {
				cardHolders.add(players.get(i));
			}
		}
		boolean agreed = false;
		while(!agreed) {
			//player says how much they'll pay
			int price = buyerJailCardAsk(buyer);
			for(int i = 0; i < cardHolders.size(); i++) {
				//then each other player is asked if they'll sell for that much
				if(sellerJailCardAccept(cardHolders.get(i), price)) {
					agreed = true;
					cardHolders.get(i).deposit(price);
					cardHolders.get(i).takeGetOutOfJailCard();
					buyer.withdraw(price);
					return true;
				}
			}
		}
		return false;
	}
	
	//asks buyer how much they'll pay
	public static int buyerJailCardAsk(Player buyer) {
		System.out.println(buyer.getName() + ", how much will you pay?");
		return InputHandler.integerInputValidation(0, buyer.getAccount());
	}
	
	//asks the seller if they accept the offer
	public static boolean sellerJailCardAccept(Player seller, int price) {
		System.out.println(seller.getName() + ", will you sell for $" + price);
		System.out.println("0 for No, 1 for Yes: ");
		int choice = InputHandler.integerInputValidation(0,1);
		if(choice == 1) {
			return true;
		}
		return false;
	}
}
