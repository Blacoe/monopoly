package menus;

public class MenuGeneration {
	public static int sizeOfLine = 52;
	
	//make window creates a box around the strings that are to be included in the menu
	//the box is however tall it needs to be to accomodate the strings
	//it is as wide as the line size variable
	public static String[] makeWindow(String title, String[] lines, int linesize) {
		String[] menuRows = new String[lines.length + 5];
		
		String topBorder = "";
		String bottomBorder = "";
		String blankSpaceLine = makeLine("", linesize);
		
		for(int i = 0; i<linesize; i++) {
			topBorder = topBorder + "_";
			if(i == 0 || i == linesize - 1) {
				bottomBorder = bottomBorder + "|";
			}
			else {
				bottomBorder = bottomBorder + "_";
			}
		}
		
		menuRows[0] = topBorder;
		menuRows[1] = makeLine(title, linesize);
		menuRows[2] = blankSpaceLine;
		
		for(int i = 0; i<lines.length; i++) {
			menuRows[3+i] = makeLine(lines[i], linesize);
		}
		
		menuRows[lines.length + 3] = blankSpaceLine;
		menuRows[lines.length + 4] = bottomBorder;
		//return the correctly formatted strings
		return menuRows;
	}
	
	//creates a neat line with end bars around a string
	public static String makeLine(String s, int lineSize) {
		int before;
		int after;
		int spaces = lineSize - s.length() - 2;
		if(spaces % 2 == 0) {
			before = spaces / 2;
			after = before;
		}
		
		else {
			before = spaces / 2;
			after = before + 1;
		}
		
		String spacesBefore = "";
		for(int i = 0; i < before; i++) {
			spacesBefore = spacesBefore + " ";
		}
		
		String spacesAfter = "";
		for(int j = 0; j < after; j++) {
			spacesAfter = spacesAfter + " ";
		}
		
		return "|" + spacesBefore + s + spacesAfter + "|";
	}
	
	//prints all of the menu lines using a loop
	public static void printMenu(String[] menuRows) {
		for(int i = 0; i<(menuRows.length); i++) {
			System.out.println(menuRows[i]);
		}
	}
}
