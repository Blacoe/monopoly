package menus;

import java.util.ArrayList;
import java.util.Scanner;

import game.Token;

// This class contains all the methods necessary for the players to set up the game of monopoly. There are 4 method in total. 

public class SetupMenus {
	
	static Scanner sc = new Scanner(System.in);
	
	// this is the initial setup interface that allows the user to choose the number of players and their names for instantiation in the main class
	public static int choosePlayers() { 
		// print setup menu
		String[] lines = new String[5];
		lines[0] = "(Enter 2) 2 Players";
		lines[1] = "(Enter 3) 3 Players";
		lines[2] = "(Enter 4) 4 Players";
		lines[3] = "(Enter 5) 5 Players";
		lines[4] = "(Enter 6) 6 Players";
		MenuGeneration.printMenu(MenuGeneration.makeWindow("CHOOSE NUMBER OF PLAYERS", lines, MenuGeneration.sizeOfLine));
		int chosenAction = InputHandler.integerInputValidation(2, 6);
		// perform input validation
		switch (chosenAction) {
		  case 2:
		    return 2;
		  case 3:
			return 3;
		  case 4:
		    return 4;
		  case 5:
		    return 5;
		  case 6:
		    return 6;
		  default:
			 System.out.print("You entered a non existent option, setting player number to 4 by default\n");
			return 4;
		}
	}

	// this is the initial setup interface that allows the user to choose the amount of money each player starts of with.
	public static int chooseStartingMoney() { 
		// print setup menu
		String[] list = new String[6];
		list[0] = "(Enter 1) $ 1000";
		list[1] = "(Enter 2) $ 1250";
		list[2] = "(Enter 3) $ 1500";
		list[3] = "(Enter 4) $ 1750";
		list[4] = "(Enter 5) $ 2000";
		list[5] = "(Enter 6) $ 2500";
		MenuGeneration.printMenu(MenuGeneration.makeWindow("CHOOSE STARTING MONEY", list, MenuGeneration.sizeOfLine));
		int chosenAction = InputHandler.integerInputValidation(1, 6);
		// perform input validation
		switch (chosenAction) {
		  case 1:
			return 1000;
		  case 2:
		    return 1250;
		  case 3:
			return 1500;
		  case 4:
		    return 1750;
		  case 5:
		    return 2000;
		  case 6:
		    return 2500;
		  default:
			 System.out.print("You entered a non existent option, setting starting money to $1500 by default\n");
			return 1500;
		}
	}
	// This method takes a string input from the user to name a player that is used to instantiate a new player object in the main function
	public static String createPlayer(int playerNumber){ 
		boolean accepted = false;
		System.out.println("What is the name of player " + playerNumber + "?");
		String playerName = "";
		while (playerName.isEmpty() || !accepted) { // this makes sure that an empty initial string created by the sc.nextLine method isn't used as a player name
			playerName = sc.nextLine();
			if(playerName.length() <= 7) {
				accepted = true;
			}
			else {
				System.out.println("Try Again, your name should not exceed 7 characters");
			}
		}
		return playerName;
	}
	
	// Finally this method assigns the chosen token to each player in the game. 
	public static Token createPlayerTokens(String playerName, ArrayList<Token> tokens) {
		ArrayList<String> options = new ArrayList<String>();
		String[] list = new String[2+tokens.size()];
		list[0] = playerName + ", choose your token!";
		list[1] = "";
		for(int i = 0; i < tokens.size(); i++) {
			list[2+i] = "(Enter " + i + ") " + tokens.get(i).toString();
			options.add("" + i);
		}
		MenuGeneration.printMenu(MenuGeneration.makeWindow(playerName.toUpperCase() + " TOKEN SELECTION", list, MenuGeneration.sizeOfLine));
		String choice = InputHandler.stringOptionsInputValidation(options);
		return tokens.remove(Integer.parseInt(choice));
	}
}
