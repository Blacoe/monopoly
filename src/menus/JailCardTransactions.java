package menus;

import java.util.ArrayList;

import game.Box;
import game.Player;

public class JailCardTransactions {
	public static void sellJailCardActionMenu(Player seller, int rolls, Box box, ArrayList<Player> players, int sourceMenu) {
		int sizePlayers = players.size();
		int sizeOtherPlayers = sizePlayers - 1;
		//if source menu is 1, this means they came from the bakruptcy menu
		//in this case, they can only sell to the bank,
		//so we skip the player list
		if(sourceMenu == 1) {
			sizePlayers = 0;
			sizeOtherPlayers = 0;
		}
		String[] list = new String[3 + sizeOtherPlayers];
		//options is passed to the input handler
		//each option is a button the player can press
		ArrayList<String> options = new ArrayList<String>();
		list[0] = "You are selling a Get Out of Jail Card";
		list[1] = "Who do you want to sell this to?";
		int c = 2;
		//a list of players to sell to is dynamically generated
		for(int i = 0; i<players.size(); i++) {
			//a list of input options for the player is also generated
			if(!players.get(i).equals(seller)) {
				list[c] = "(Enter " + i + ")" + players.get(i).getName();
				options.add("" + i);
				c++;
			}
		}
		list[c] = "(Enter 7) Sell to the Bank";
		options.add("" + 7);
		MenuGeneration.printMenu(MenuGeneration.makeWindow("CARD SALE", list, MenuGeneration.sizeOfLine));
		//take the user's choice using the input handler
		String choice = InputHandler.stringOptionsInputValidation(options);
		int choiceInt = Integer.parseInt(choice);
		if(choiceInt == 7) {
			//they chose to sell to bank
			seller.takeGetOutOfJailCard();
			System.out.println("You sold a Jail-Break card for $50");
			seller.deposit(50);
		}
		else {
			//they are selling to another player
			Player buyer = players.get(choiceInt);
			cardTransactionMenu(seller, buyer, rolls, box, sourceMenu, players);
		}
	}
	
	//if they choose to sell to another player
	public static boolean cardTransactionMenu(Player seller, Player buyer, int rolls, Box box, int sourceMenu, ArrayList<Player> players) {
		String[] list = new String[6];
		list[0] = buyer.getName() + "!";
		list[1] = seller.getName() + " wants to sell a Get Out of Jail Card";
		list[2] = "Do you accept?";
		list[3] = "";
		list[4] = "(Enter 1) Yes";
		list[5] = "(Enter 2) No";
		MenuGeneration.printMenu(MenuGeneration.makeWindow("CARD OFFER", list, MenuGeneration.sizeOfLine));
		//first player says if they want to buy
		int choice = InputHandler.integerInputValidation(1,2);
		//if not then exit the method
		if(choice == 2) {
			String[] list2 = new String[1];
			list2[0] = "Sorry " + seller.getName() + ", your offer was declined";
			MenuGeneration.printMenu(MenuGeneration.makeWindow("OFFER DECLINED", list2, MenuGeneration.sizeOfLine));
			return false;
		}
		boolean decisionSeller = false;
		boolean decisionBuyer = false;
		int sellerPrice = 0;
		//if they do want to buy, start a loop until a decision is reached
		while((!decisionSeller)||(!decisionBuyer)) {
			String[] list3 = new String[3];
			list3[0] = seller.getName() + ", name your price";
			list3[1] = "";
			list3[2] = "(Enter -1) Exit Deal";
			//seller player names their price
			MenuGeneration.printMenu(MenuGeneration.makeWindow("MAKE OFFER", list3, MenuGeneration.sizeOfLine));
			sellerPrice = InputHandler.integerInputValidation(-1, buyer.getAccount());
			if(sellerPrice == -1) {
				return false;
			}
			decisionSeller = true;
			
			String[] list4 = new String[5];
			list4[0] = buyer.getName() + ", do you accept:";
			list4[1] = "The Card for $" + sellerPrice;
			list4[2] = "";
			list4[3] = "(Enter 1) Accept";
			list4[4] = "(Enter 2) Decline";
			//buyer player accepts or declines the offer
			MenuGeneration.printMenu(MenuGeneration.makeWindow("ACCEPT OFFER", list4, MenuGeneration.sizeOfLine));
			int buyerChoice = InputHandler.integerInputValidation(1,2);
			if(buyerChoice == 1) {
				//decision reached, exit method and give card
				decisionBuyer = true;
				String[] list5 = new String[2];
				list5[0] = buyer.getName() + " congratulations on your new card!";
				list5[1] = "Card SOLD for $" + sellerPrice;
				MenuGeneration.printMenu(MenuGeneration.makeWindow("SALE AGREED", list5, MenuGeneration.sizeOfLine));
				buyer.withdraw(sellerPrice);
				seller.takeGetOutOfJailCard();
				seller.deposit(sellerPrice);
				buyer.giveGetOutOfJailCard();
				return true;
			}
		}
		return false;
	}
}
