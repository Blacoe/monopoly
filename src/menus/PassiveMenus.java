package menus;

// Each of these menus method are used to display information to the player without requiring an input other than 
// prompting the enter key to continue. All credits for the ASCII art goes to: https://www.asciiart.eu/
// The menu's included are:
// 			- Landing on the GO tile.
//			- Landing on the Income Tax tile. 
//			- Landing on the Free Parking tile.
// 			- Landing on the Jail (just visiting) tile. 
//			- Landing on the Go to Jail tile.
//			- Showing the list of winners.
//			- Landing on a property owned by the player. 

import java.util.ArrayList;

import game.Player;
import tiles.Property;

public class PassiveMenus {
	public static void goTile() {
		String[] goStrings = new String[10];
		goStrings[0] = ",ad8888ba,     ,ad8888ba,";
		goStrings[1] = "d8\"'    `\"8b   d8\"'    `\"8b";
		goStrings[2] = "d8'            d8'        `8b";
		goStrings[3] = "88             88          88";
		goStrings[4] = "88      88888  88          88";
		goStrings[5] = "Y8,        88  Y8,        ,8P";
		goStrings[6] = "Y8a.    .a88   Y8a.    .a8P";
		goStrings[7] = "`\"Y88888P\"     `\"Y8888Y\"'";
		goStrings[8] = " ";
		goStrings[9] = "You landed on GO!";
		String[] goWindow = MenuGeneration.makeWindow("GO", goStrings , MenuGeneration.sizeOfLine);
		MenuGeneration.printMenu(goWindow);
		InputHandler.promptEnterKey();
	}
	public static void IncomeTax(Player p, int tax) {
		String[] taxStrings = new String[11];
		taxStrings[0] = ",-==-,      ";
		taxStrings[1] = "\\/\\/\\/      ";
		taxStrings[2] = "\\\\//      ";
		taxStrings[3] = ".-''-.      ";
		taxStrings[4] = ".'.-\"\"-.'.---. ";
		taxStrings[5] = "/ /    .'\\ \\--.'.";
		taxStrings[6] = "| |   / /| |   \\ \\";
		taxStrings[7] = "'.'-: \\ \\.'    / /";
		taxStrings[8] = "'---; '-..-'.'";
		taxStrings[9] = "     `'----'";
		taxStrings[10] = p.getName()+" owes "+tax+" to the bank!";
		String[] goWindow = MenuGeneration.makeWindow("INCOME TAX", taxStrings , MenuGeneration.sizeOfLine);
		MenuGeneration.printMenu(goWindow);
		InputHandler.promptEnterKey();
	}
	   
	public static void freeParking() {
		String[] strings = new String[6];
		strings[0] = "   -           __                ";
		strings[1] = " --          ~( @\\   \\           ";
		strings[2] = "---   _________]_[__/_>________  ";
		strings[3] = "     /  ____ \\ <>     |  ____  \\ ";
		strings[4] = "    =\\_/ __ \\_\\_______|_/ __ \\__D";
		strings[5] = "________(__)_____________(__)____";
		MenuGeneration.printMenu(MenuGeneration.makeWindow("FREE PARKING", strings, MenuGeneration.sizeOfLine));
		InputHandler.promptEnterKey();
	}
	
	public static void landJailMenu() {
		String[] strings = new String[8];
		strings[0] = "__  _.-\"` `'-.";
		strings[1] = "/||\\'._ __{}_(";
		strings[2] = "||||  |'--.__\\";
		strings[3] = "|  L.(   ^_\\^";
		strings[4] = "\\ .-' |   _ |";
		strings[5] = "| |   )\\___/";
		strings[6] = " |  \\-'`:._]";
		strings[7] = "     \\__/;";
		MenuGeneration.printMenu(MenuGeneration.makeWindow("JAIL: JUST VISITING", strings, MenuGeneration.sizeOfLine));
		InputHandler.promptEnterKey();
	}
	
	public static void goToJailMenu() {
		String[] strings = new String[8];
		strings[0] = "__  _.-\"` `'-.";
		strings[1] = "/||\\'._ __{}_(";
		strings[2] = "||||  |'--.__\\";
		strings[3] = "|  L.(   ^_\\^";
		strings[4] = " \\ .-' |   _ |";
		strings[5] = "| |   )\\___/";
		strings[6] = "|  \\-'`:._]";
		strings[7] = "     \\__/;";
		MenuGeneration.printMenu(MenuGeneration.makeWindow("GO TO JAIL", strings, MenuGeneration.sizeOfLine));
		InputHandler.promptEnterKey();
	}
	
	public static void winners(ArrayList<Player> winningPlayerList) {
		int n = winningPlayerList.size();
		String[] winnerStrings = new String[11+n];
		winnerStrings[0] = "___________";
		winnerStrings[1] = "'._==_==_=_.'";
		winnerStrings[2] = ".-\\:      /-.";
		winnerStrings[3] = "| (|:.     |) |";
		winnerStrings[4] = "'-|:.     |-'";
		winnerStrings[5] = "\\::.    /";
		winnerStrings[6] = " '::. .'";
		winnerStrings[7] = ") (";
		winnerStrings[8] = "_.' '._";
		winnerStrings[9] = "`\"\"\"\"\"\"\"`";
		winnerStrings[10] = " ";
		for(int i = 0;i<n;i++){
			int position = i+1;
				winnerStrings[11+i] = position + ": " + winningPlayerList.get(i).getName();
		}
		String[] winners = MenuGeneration.makeWindow("WINNERS", winnerStrings, MenuGeneration.sizeOfLine);
		MenuGeneration.printMenu(winners);
	}
	
	public static void justVisitingOwnPropertyMenu(Player p, Property prop) {
		String[] strings = new String[8];
		strings[0] = "':.";
		strings[1] = "[]______";
		strings[2] = "/\\       \\";
		strings[3] = "---/\\___\\ | '''''' |__\\-- ---";
		strings[4] = "||'''| |''||''|''|";
		strings[5] = "``\"\"\"`\"`\"\"))\"\"`\"\"`";
		strings[6] = "";
		strings[7] = "Just visiting!";

		MenuGeneration.printMenu(MenuGeneration.makeWindow(prop.getName().toUpperCase(), strings, MenuGeneration.sizeOfLine));
		InputHandler.promptEnterKey();
	}
}
