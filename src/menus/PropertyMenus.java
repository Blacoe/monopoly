package menus;

import java.util.ArrayList;

import game.Box;
import game.Player;
import tiles.Property;
import tiles.Street;

// This is the class that is executed when a player wants to view their properties. It contains 5 methods in order to achieve this. 
// It shows the list of properties owned by the player first by using the propertyMenu() method. 

// It shows the options available to the player by using logic. These options include going back, or viewing more information about a specific property. 

// There is dynamic logic implemented to show the player whether or not a property can be developed (build house or hotel). If the player
// selects a property it calls the titleDeedsCardMenu(), which is explained below.

public class PropertyMenus {
	public static void propertyMenu(Player p, int rolls, Box box, int sourceMenu, ArrayList<Player> players) {
		int propertyChoice = 1;
		while(propertyChoice!=0) {
			if(p.getProperties().isEmpty()) {
				String[] noPropMessage = {"You don't have any properties...", "", "YET!","(Enter 0) Go Back"};
				MenuGeneration.printMenu(MenuGeneration.makeWindow(p.getName().toUpperCase() + "'S PROPERTIES", noPropMessage, MenuGeneration.sizeOfLine));
				propertyChoice = InputHandler.integerInputValidation(0,0);
			}
			else {
				String[] properties = p.toStringPropertyList();
				String[] propertiesWithOptionLine = new String[properties.length+5];
				String developments = "";
				propertiesWithOptionLine[0] = "Enter Property Number to View Info and Interact";
				propertiesWithOptionLine[1] = "'+' indicates properties that can be developed";
				propertiesWithOptionLine[2] = "";
				for(int i = 0; i < properties.length; i++) {
					if(p.getProperties().get(i) instanceof Street) {
						if(((Street)p.getProperties().get(i)).getCanBuildHouse() && p.canAfford(((Street)p.getProperties().get(i)).getHousePrice())) {
							developments = " +";
						}
						if(((Street)p.getProperties().get(i)).getCanBuildHotel() && p.canAfford(((Street)p.getProperties().get(i)).getHotelPrice())) {
							developments = " +";
						}
					}
					propertiesWithOptionLine[i+3] = properties[i] + developments;
					developments = "";
				}		
				propertiesWithOptionLine[properties.length+3] = "";
				propertiesWithOptionLine[properties.length+4] = "(Enter 0) Go Back";
				String[] menuRows = MenuGeneration.makeWindow(p.getName().toUpperCase() + "'S PROPERTIES", propertiesWithOptionLine, MenuGeneration.sizeOfLine);
				MenuGeneration.printMenu(menuRows);
				propertyChoice = InputHandler.integerInputValidation(0, properties.length);
				if(propertyChoice!=0) {
					titleDeedsCardMenu(p, propertyChoice, rolls, box, sourceMenu, players);			
				}
			}
		}
	}
	
	// This method show the player information about the selected property, including: The name of the property, the amount of houses
	// and hotels on the property, whether or not the player can build on the property, an option to sell houses, hotels and the property
	// itself. It also includes logic checks to see if a player can afford all of these options. 
	
	// If the player chooses to sell the property then the third method in this class is called (sellPropertyActionMenu()) explained below.
	
	public static void titleDeedsCardMenu(Player p, int propertyChoice, int rolls, Box box, int sourceMenu, ArrayList<Player> players) {
		String choice = "x";
		while(!choice.equals("b")) {
			ArrayList<String> options = new ArrayList<String>();
			ArrayList<String> charEntries = new ArrayList<String>();
			Property property = p.getProperties().get(propertyChoice-1);
			if(property instanceof Street) {
				if(((Street) property).getCanBuildHouse() && p.canAfford(((Street) property).getHousePrice()) && !box.getHouseShortage()) {
					charEntries.add("q");
					options.add("(Enter q) Buy House");
				}
				if(((Street) property).getNumHouses() > 0) {
					charEntries.add("w");
					options.add("(Enter w) Sell House");
				}
				if(((Street) property).getCanBuildHotel() && p.canAfford(((Street) property).getHotelPrice()) && !box.getHotelShortage()) {
					charEntries.add("e");
					options.add("(Enter e) Buy Hotel");
				}
				if(((Street) property).getNumHotels() > 0) {
					charEntries.add("r");
					options.add("(Enter r) Sell Hotel");
				}
				if(((Street) property).getNumHouses() == 0 && ((Street) property).getNumHotels() == 0) {
					options.add("(Enter t) Sell this Property");
					charEntries.add("t");
				}

			}
			else {
				options.add("(Enter t) Sell this Property");
				charEntries.add("t");
			}
			options.add("(Enter b) Go Back");
			charEntries.add("b");
			String[] propertyString = property.toStringList();
			String[] propertyStringWithOption = new String[propertyString.length+1+options.size()];
			for(int i = 0; i < propertyString.length; i++) {
				propertyStringWithOption[i] = propertyString[i];
			}
			propertyStringWithOption[propertyString.length] = "";
			int n = 0;
			for (int i = 0; i < options.size(); i++) {
				propertyStringWithOption[i+propertyString.length + 1] = options.get(n);
				n++;
			}
			MenuGeneration.printMenu(MenuGeneration.makeWindow(property.getName().toUpperCase(), propertyStringWithOption, MenuGeneration.sizeOfLine));
			choice = InputHandler.stringOptionsInputValidation(charEntries);
			if(choice.equals("q")) {
				if(!p.buyHouse((Street)property, box)) {
					System.out.println("Sorry, you can't afford this right now");
				}
			}
			if(choice.equals("w")) {
				if(!p.sellHouse((Street)property, box)) {
					System.out.println("Sorry, you can't sell this right now");
				}
			}
			if(choice.equals("e")) {
				if(!p.buyHotel((Street)property, box)) {
					System.out.println("Sorry, you can't afford this right now");
				}
			}
			if(choice.equals("r")) {
				if(!p.sellHotel((Street)property, box)) {
					System.out.println("Sorry, you can't sell this right now");
				}
			}
			if(choice.equals("t")) {
				sellPropertyActionMenu(p, players, property, propertyChoice, rolls, box, sourceMenu);
				choice = "b";
			}
		}
	}
	
// ACTION MENUS: These methods handle property sales between players and the bank.
	
	// This menu gives the player the option to sell a property to another player or the bank. 
	public static void sellPropertyActionMenu(Player seller, ArrayList<Player> players, Property p, int propertyChoice, int rolls, Box box, int sourceMenu) {
		int sizePlayers = players.size();
		int sizeOtherPlayers = sizePlayers - 1;
		if(sourceMenu == 1) {
			sizePlayers = 0;
			sizeOtherPlayers = 0;
		}
		String[] list = new String[3 + sizeOtherPlayers];
		ArrayList<String> options = new ArrayList<String>();
		list[0] = "You are selling: " + p.getName();
		list[1] = "Who do you want to sell this to?";
		int c = 2;
		for(int i = 0; i<sizePlayers; i++) {
			if(!players.get(i).equals(seller)) {
				list[c] = "(Enter " + i + ") " + players.get(i).getName();
				options.add("" + i);
				c++;
			}
		}
		list[c] = "(Enter 7) Sell to the Bank";
		options.add("" + 7);
		MenuGeneration.printMenu(MenuGeneration.makeWindow("PROPERTY SALE", list, MenuGeneration.sizeOfLine));
		String choice = InputHandler.stringOptionsInputValidation(options);
		int choiceInt = Integer.parseInt(choice);
		if(choiceInt == 7) {
			seller.sellPropertyToBank(p);
			bankSoldMenu(p); // If the player chooses 7 then the sellPropertyToBank() method is called which show the bankSoldMenu() below. 
		}
		else {
			Player buyer = players.get(choiceInt);
			propertyTransactionMenu(seller, buyer, p, propertyChoice, rolls, box, sourceMenu, players);
			// Otherwise the player has selected another player and the propertyTransactionMenu() method below is executed. 
		}
	}
	
	// This method show a menu to the player informing them they have sold a property to the bank, and the amount it was sold for. 
	public static void bankSoldMenu(Property prop) {
		String[] list = new String[3];
		list[0] = prop.getName();
		list[1] = "SOLD!";
		list[2] = "to the Bank, for $" + (prop.getPrice()/2);
		MenuGeneration.printMenu(MenuGeneration.makeWindow("BANK AUCTION", list, MenuGeneration.sizeOfLine));
		InputHandler.promptEnterKey();
	}
	
	// This method show handles the negotiation process between two players after the original player proposes to sell a property to another player. 
	public static boolean propertyTransactionMenu(Player seller, Player buyer, Property p, int propertyChoice, int rolls, Box box, int sourceMenu, ArrayList<Player> players) {
		String[] list = new String[6];
		list[0] = buyer.getName() + "!";
		list[1] = seller.getName() + " wants to sell " + p.getName();
		list[2] = "Do you accept?";
		list[3] = "";
		list[4] = "(Enter 1) Yes";
		list[5] = "(Enter 2) No";
		// First the other player is informed of the offer and they may accept, continuing the process, or decline returning to the original player. 
		MenuGeneration.printMenu(MenuGeneration.makeWindow("PROPERTY OFFER", list, MenuGeneration.sizeOfLine));
		int choice = InputHandler.integerInputValidation(1,2);
		if(choice == 2) {
			String[] list2 = new String[1];
			list2[0] = "Sorry " + seller.getName() + ", your offer was declined";
			MenuGeneration.printMenu(MenuGeneration.makeWindow("OFFER DECLINED", list2, MenuGeneration.sizeOfLine));
			return false;
		}
		boolean decisionSeller = false;
		boolean decisionBuyer = false;
		int sellerPrice = 0;
		while((!decisionSeller)||(!decisionBuyer)) {
			String[] list3 = new String[3];
			list3[0] = seller.getName() + ", name your price";
			list3[1] = "";
			list3[2] = "(Enter -1) Exit Deal";
			// If the offer to negotiate is accepted then party 2 is entitled to now make an offer to the original player. 
			MenuGeneration.printMenu(MenuGeneration.makeWindow("MAKE OFFER", list3, MenuGeneration.sizeOfLine));
			sellerPrice = InputHandler.integerInputValidation(-1, buyer.getAccount());
			if(sellerPrice == -1) {
				return false;
			}
			decisionSeller = true;
			
			String[] list4 = new String[5];
			list4[0] = buyer.getName() + ", do you accept:";
			list4[1] = p.getName() + " for $" + sellerPrice;
			list4[2] = "";
			list4[3] = "(Enter 1) Accept";
			list4[4] = "(Enter 2) Decline";
			MenuGeneration.printMenu(MenuGeneration.makeWindow("ACCEPT OFFER", list4, MenuGeneration.sizeOfLine));
			int buyerChoice = InputHandler.integerInputValidation(1,2);
			// If the offer is accepting by both players the transaction commences and the logic to change its ownership is implemented. 
			if(buyerChoice == 1) {
				decisionBuyer = true;
				String[] list5 = new String[2];
				list5[0] = buyer.getName() + " congratulations on your new home!";
				list5[1] = p.getName() + ": SOLD for $" + sellerPrice;
				MenuGeneration.printMenu(MenuGeneration.makeWindow("SALE AGREED", list5, MenuGeneration.sizeOfLine));
				buyer.withdraw(sellerPrice);
				seller.removeProperty(p);
				seller.deposit(sellerPrice);
				buyer.addProperty(p);
			}
		}
		return false;
	}
}
