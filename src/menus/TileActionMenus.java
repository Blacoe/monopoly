package menus;

import java.util.Arrays;

import cards.Card;
import game.Dice;
import game.Player;
import tiles.Property;

public class TileActionMenus {
	
	//this shows the card text with a box around it
	public static void cardMenu(Card c, boolean isChance) {
		String cardText = c.getText();
		String title = "COMMUNITY CHEST";
		int lineNumber = 0;
		int charsThisLine = 0;
		if(isChance) {
			title = "CHANCE";
		}
		// roughly 1/2 the menu should be card text
		int numberOfCharsPerLine = MenuGeneration.sizeOfLine/2;
		
		String[] cardWords = cardText.split(" ");
		//worst case scenario is one word per line
		String[] cardLines = new String[cardWords.length];
		cardLines[lineNumber] = "";
		
		//the words have to be split apart, and each word has to be decided
		//on whether it will fit into a given line
		//if it doesn't fit, move to a new line
		for(int i = 0; i<cardWords.length; i++) {
			if(charsThisLine + cardWords[i].length() > numberOfCharsPerLine) {
				lineNumber++;
				cardLines[lineNumber] = "";
				charsThisLine = 0;
				cardLines[lineNumber] = cardLines[lineNumber] + cardWords[i] + " ";
				charsThisLine = charsThisLine + cardWords[i].length() + 1;
			}
			else {
				cardLines[lineNumber] = cardLines[lineNumber] + cardWords[i] + " ";
				charsThisLine = charsThisLine + cardWords[i].length() + 1;
			}
		}
		
		//don't take the whole array or there will be null pointers
		cardLines = Arrays.copyOfRange(cardLines, 0, lineNumber + 1);
		String[] menuRows = MenuGeneration.makeWindow(title, cardLines, MenuGeneration.sizeOfLine);
		MenuGeneration.printMenu(menuRows);
		InputHandler.promptEnterKey();
	}
	
	//when a player lands on a property they have to decide whether they buy it
	//if this returns true, they buy
	//if false, then auction
	public static boolean propertyActionMenu(Player p, boolean canAfford, Property prop) {
		if (canAfford) {
			String[] list = new String[5];
			list[0] = prop.getName() + " for Sale: $" + prop.getPrice();
			list[1] = "(Enter 1) Buy";
			list[2] = "(Enter 2) Auction";
			list[3] = "";
			list[4] = "You have: $" + p.getAccount();
			MenuGeneration.printMenu(MenuGeneration.makeWindow(p.getName().toUpperCase() + "'S ACTION", list, MenuGeneration.sizeOfLine));
			int chosenAction = InputHandler.integerInputValidation(1, 2);
				// perform input validation
				switch (chosenAction) {
				  case 1:
				    return true;
				  case 2:
					return false;
				  default:
					return false;
				}
			}
		else {
		System.out.print("You cannot afford this property, it's going to auction!\n");
		return false;
		}
		// action for buying property or instead auction, ask player if they want to buy or auction and return true if want to buy
	}
	
	//if the player lands on someone else's property this pops up
	//the logic in Property handles the actual transaction
	public static void payRentActionMenu(Player p, Property prop, Dice dice) {
		String[] list = new String[4];
		list[0] = "You landed on " + prop.getName();
		list[1] = "You must pay " + prop.calculateRent(dice) + " to " + prop.getOwner().getName();
		list[2] = "";
		list[3] = "You have: " + p.getAccount();
		MenuGeneration.printMenu(MenuGeneration.makeWindow("PAY RENT", list, MenuGeneration.sizeOfLine));
		InputHandler.promptEnterKey();
	}
}
