package menustests;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import game.Box;
import game.Player;
import menus.JailCardTransactions;

class JailCardTransacionsTest {

	@Test
	void testJailCardTransactions() {
		Box box = Box.getInstance(null, null);
		Player p1 = new Player("Jimbo", null, 100);
		Player p2 = new Player("Jim" ,null, 100);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p1);
		players.add(p2);
		JailCardTransactions.cardTransactionMenu(p1, p2, 1, box, 0, players);
	}

}
