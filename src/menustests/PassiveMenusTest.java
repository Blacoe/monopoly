package menustests;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import game.Box;
import game.Player;
import game.Token;
import menus.JailMenus;
import menus.PassiveMenus;
import tiles.Property;

class PassiveMenusTest {

	@Test 
	void testInJailMenu() {
		Player player1 = new Player("Jim", new Token("dark", "blue"), 100);
		Property p = new Property("brown place", 20, 300, 15);
		player1.addProperty(p);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		JailMenus.inJailMenu(player1, players, Box.getInstance(null, null));
	}
	
	@Test
	void testGoMenu() {
		PassiveMenus.goTile();
	}
	
	@Test
	void testTaxMenu() {
		Player player1 = new Player("Jim", new Token("dark", "blue"), 100);
		PassiveMenus.IncomeTax(player1, 200);
	}

}
