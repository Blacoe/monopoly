package menustests;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import game.Box;
import game.Player;
import game.Token;
import menus.PropertyMenus;
import tiles.Property;
import tiles.Street;

class PropertyMenusTest {

	@Test // options being tested: go back, go back, roll dice
	void testTitleDeedsCardMenu() {
		Box box = Box.getInstance(null, null);
		Player player1 = new Player("Jim", new Token("dark", "blue"), 100);
		Property p = new Property("brown place", 20, 300, 15);
		player1.addProperty(p);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		int rolls = 0;
		PropertyMenus.titleDeedsCardMenu(player1, 1, rolls, box, 0, players);
	}
	
	@Test 
	void testPropertyList() {
		Box box = Box.getInstance(null, null);
		int[] rentHouses = {200, 300, 400, 500, 1000};
		Player p = new Player("Jim", new Token("dark", "blue"), 1000);
		Property washSt = new Street("Washington Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Property brianSt = new Street("Brian Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Property broadSt = new Street("Broad Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		p.addProperty(washSt);
		p.addProperty(brianSt);
		p.addProperty(broadSt);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		PropertyMenus.propertyMenu(p, 1, box, 0, players);
	}
}
