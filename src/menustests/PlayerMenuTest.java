package menustests;

import java.util.ArrayList;

import game.*;
import menus.PassiveMenus;
import menus.PlayerMenu;

import org.junit.jupiter.api.Test;

class PlayerMenuTest {
	
	@Test 
	void testPlayerSort() {
		Player player1 = new Player("Jim", null, 100);
		Player player2 = new Player("Jim2", null, 50000);
		Player player3 = new Player("Jim3", null, 2);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		players.add(player3);
		Game.sortPlayers(players);
		PassiveMenus.winners(players);
	}
	
	@Test 
	void testStartTurnMenu() {
		Box box = Box.getInstance(null, null);
		Player player1 = new Player("Jim", new Token("dark", "blue"), 100);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		int rolls = 0;
		PlayerMenu.startTurnMenu(player1, rolls, box, players);
	}
	
}
