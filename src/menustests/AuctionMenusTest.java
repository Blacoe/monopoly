package menustests;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import game.Box;
import game.Player;
import menus.AuctionMenus;
import tiles.Property;

class AuctionMenusTest {

	@Test
	void testAuctionMenu() {
		Box box = Box.getInstance(null, null);
		Player p1 = new Player("Jimbo", null, 100);
		Player p2 = new Player("Jim" ,null, 100);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p1);
		players.add(p2);
		Property p = new Property("Texas", 100, 100, 100);
		AuctionMenus.auctionMenu(players, p, box);
	}

}
