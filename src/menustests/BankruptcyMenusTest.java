package menustests;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import game.Box;
import game.Player;
import game.Token;
import tiles.Property;

class BankruptcyMenusTest {

	@Test
	void testBankruptcyActionMenu() {
		Box box = Box.getInstance(null, null);
		Player player1 = new Player("Jim", new Token("dark", "blue"), 100);
		Property p = new Property("brown place", 20, 300, 15);
		player1.addProperty(p);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		player1.bankruptcyAction(true, 110, player1, box, players);
	}

}
