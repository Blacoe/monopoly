package menustests;

import org.junit.jupiter.api.Test;

import cards.Card;
import menus.TileActionMenus;

class TileActionMenusTest {

	@Test
	void testCardMenu() {
		Card c = new Card("This is some really long text that tests the card menu method to see if it is good");
		TileActionMenus.cardMenu(c, true);
	}

}
