package menustests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import menus.MenuGeneration;

class MenuGenerationTest {

	@Test
	void testMakeLine() {
		String s = "(Enter 1) Options";
		//s has length 17
		String line = MenuGeneration.makeLine(s, 52);
		assertEquals(line, "|                (Enter 1) Options                 |");
	}
	
	@Test
	void testMakeWindow() {
		String[] lines = {"Dog", "Cat", "Mouse"};
		String[] menuRows = MenuGeneration.makeWindow("RANDOM MENU", lines, 52);
		for(int i = 0; i<(lines.length+5); i++) {
			System.out.println(menuRows[i]);
		}
		MenuGeneration.printMenu(menuRows);
	}

}
