package menustests;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import game.Player;
import menus.JailMenus;

class JailMenusTest {

	@Test
	void testSeekJailCard() {
		Player p1 = new Player("Jimbo", null, 100);
		Player p2 = new Player("Jim" ,null, 100);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p1);
		players.add(p2);
		JailMenus.seekJailCard(p1, players);
	}

}
