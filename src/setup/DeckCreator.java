package setup;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import cards.*;
import game.Deck;

public class DeckCreator {
    
	//creates deck from a text file
	public static Deck createDeck(String filename) {		
		String line;
        File file = new File(filename);
        ArrayList<Card> cardList = new ArrayList<Card>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
            	//loops through each line of file
                line = scanner.nextLine();
                //create one card out of each line
                Card card = createCard(line);
                if(!card.equals(null)) {
                    cardList.add(card);
                }
                //skip card if not recognized
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }   
        Deck deck = new Deck(cardList);
        return deck;
    }
	
	//creates one card from one line of text
	public static Card createCard(String str) {
		//fields are ';' separated
		String[] fields = str.split(";", -1);
		Card card;
	        switch(fields[0]) 
	        { 
	            case "AdvanceToNearestCard": 
	            	card = new AdvanceToNearestCard(fields[1], fields[2]);
	                break; 
	            case "CollectFromEachPlayerCard": 
	            	card = new CollectFromEachPlayerCard(fields[1], Integer.parseInt(fields[2]));
	                break; 
	            case "CollectMoneyCard": 
	            	card = new CollectMoneyCard(fields[1], Integer.parseInt(fields[2]));
	                break;
	            case "GetOutOfJailCard":
	            	card = new GetOutOfJailCard(fields[1]); 
	            	break;
	            case "GoToCard":
	            	card = new GoToCard(fields[1], Integer.parseInt(fields[2]));
	            	break;
	            case "HouseFeeCard":
	            	card = new HouseFeeCard(fields[1], Integer.parseInt(fields[2]), Integer.parseInt(fields[3]));
	            	break;
	            case "JumpToCard":
	            	card = new JumpToCard(fields[1], Integer.parseInt(fields[2]));
	            	break;
	            case "MovePlacesCard":
	            	card = new MovePlacesCard(fields[1], Integer.parseInt(fields[2]));
	            	break;
	            case "PayEachPlayerCard":
	            	card = new PayEachPlayerCard(fields [1], Integer.parseInt(fields[2]));
	            	break;
	            case "TaxCard":
	            	card = new TaxCard(fields[1], Integer.parseInt(fields[2]));
	            	break;
	            default: 
	            	card = null;
	            	break;
	        } 
		return card;
	}
	
}