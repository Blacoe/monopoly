package setup;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import game.Gameboard;
import tiles.CardTile;
import tiles.FreeParking;
import tiles.GoTile;
import tiles.GoToJail;
import tiles.IncomeTax;
import tiles.Jail;
import tiles.Railroad;
import tiles.Street;
import tiles.Tile;
import tiles.Utilities;

public class GameboardCreator {
    
	//this reads from a file to create the gameboard
    public static Gameboard createGameboard(String filename) {		
		String line;
        File file = new File(filename);
        ArrayList<Tile> tileList = new ArrayList<Tile>();
        try {
            Scanner scanner = new Scanner(file);
            //each line of the file is a new tile
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                //create each tile from string
                Tile tile = createTile(line);
                if(!tile.equals(null)) {
                    tileList.add(tile);
                }
                //skip card if not recognized
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }   
        Gameboard gameboard = new Gameboard(tileList);
        return gameboard;
    }
	
    //this method creates one tile from one string (line in the file)
	public static Tile createTile(String str) {
		//fields are ';' separated
		String[] fields = str.split(";", -1);
		Tile tile;
	        switch(fields[0]) 
	        { 
	        //switch on fields and create each tile
	            case "CardTile": 
	            	boolean isChance = false;
	            	if(fields[3].equalsIgnoreCase("true")) {
	            		isChance = true;
	            	}
	            	tile = new CardTile(fields[1], Integer.parseInt(fields[2]), isChance);
	                break; 
	            case "FreeParking": 
	            	tile = new FreeParking(fields[1], Integer.parseInt(fields[2]));
	                break; 
	            case "GoTile": 
	            	tile = new GoTile(fields[1], Integer.parseInt(fields[2]));
	                break; 
	            case "GoToJail": 
	            	tile = new GoToJail(fields[1], Integer.parseInt(fields[2]));
	                break; 
	            case "IncomeTax": 
	            	tile = new IncomeTax(fields[1], Integer.parseInt(fields[2]), Integer.parseInt(fields[3]));
	                break; 
	            case "Jail": 
	            	tile = new Jail(fields[1], Integer.parseInt(fields[2]));
	                break; 
	            case "Railroad": 
	            	tile = new Railroad(fields[1], Integer.parseInt(fields[2]), Integer.parseInt(fields[3]), Integer.parseInt(fields[4]));
	                break; 
	            case "Street": 
	            	String name = fields[1];
	            	int position = Integer.parseInt(fields[2]);
	            	int price = Integer.parseInt(fields[3]);
	            	String colour = fields[4];
	            	int rent = Integer.parseInt(fields[5]);
	            	String[] rentHousesString = fields[6].split("-", -1);
	            	int[] rentHouses = {Integer.parseInt(rentHousesString[0]), Integer.parseInt(rentHousesString[1]), Integer.parseInt(rentHousesString[2]), Integer.parseInt(rentHousesString[3]), Integer.parseInt(rentHousesString[4])};
	            	int housePrice = Integer.parseInt(fields[7]);
	            	int hotelPrice = Integer.parseInt(fields[8]);
	            	tile = new Street(name, position, price, colour, rent, rentHouses, housePrice, hotelPrice);
	                break; 
	            case "Utilities": 
	            	tile = new Utilities(fields[1], Integer.parseInt(fields[2]), Integer.parseInt(fields[3]), Integer.parseInt(fields[4]));
	                break; 
	            default: 
	            	tile = null;
	            	break;
	        } 
		return tile;
	}
}
