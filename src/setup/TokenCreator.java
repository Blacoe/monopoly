package setup;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import game.Token;

// This setup class creates token arrayList using an input file that specifies the color and name of each token. 
// In our project this text file is called 'CustomTokens.txt'
public class TokenCreator {
	
	public static ArrayList<Token> tokenCreator(String filename){
		String line;
        File file = new File(filename);
        ArrayList<Token> tokens = new ArrayList<Token>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                Token token = makeSingleToken(line);
                if(!token.equals(null)) {
                    tokens.add(token);
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }   
        return tokens;
	}
	
	public static Token makeSingleToken(String str) {
		String[] fields = str.split(";", -1);
		return new Token(fields[0], fields[1]);
	}
}
