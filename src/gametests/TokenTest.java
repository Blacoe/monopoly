package gametests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import game.Token;

class TokenTest {

	@Test
	void testGetName() {
		Token token = new Token("Kangaroo", "Brown");
		assertTrue(token.getName().equals("Kangaroo"));
	}
	
	@Test
	void testGetColour() {
		Token token = new Token("Kangaroo", "Brown");
		assertTrue(token.getName().equals("Brown"));
	}
	
	@Test
	void testGetAssignmentStatus() {
		Token token = new Token("Kangaroo", "Brown");
		token.setAssignmentStatus(1);
		assertEquals(1, token.getAssignmentStatus());
	}
	
	@Test
	void testToString() {
		Token token = new Token("Kangaroo", "Brown");
		assertTrue(token.getName().equals("Brown Kangaroo"));
	}
}
