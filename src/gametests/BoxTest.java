package gametests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import cards.Card;
import game.Box;
import game.Deck;

class BoxTest {

	@Test
	void communityChestDeckTest() {
		
		ArrayList<Card> testCardsCommunityChest = new ArrayList<Card>();
		ArrayList<Card> testCardsChance = new ArrayList<Card>();
		
		Card card1 = new Card("Go to jail");
		Card card2 = new Card("go back");
		Card card3 = new Card("go forward");
		Card card4 = new Card("its you birthday");
		Card card5 = new Card("go to go");
		
		testCardsCommunityChest.add(card1);
		testCardsCommunityChest.add(card2);
		testCardsCommunityChest.add(card3);
		testCardsCommunityChest.add(card4);
		testCardsCommunityChest.add(card5);
		
		Deck testDeckCommunityChest = new Deck(testCardsCommunityChest);
		
		Card cardChance1 = new Card("Go to jail Chance");
		Card cardChance2 = new Card("go back Chance");
		Card cardChance3= new Card("go forward Chance");
		Card cardChance4 = new Card("its you birthday Chance");
		Card cardChance5 = new Card("go to go Chance");
		
		testCardsChance.add(cardChance1);
		testCardsChance.add(cardChance2);
		testCardsChance.add(cardChance3);
		testCardsChance.add(cardChance4);
		testCardsChance.add(cardChance5);
		
		Deck testDeckChance = new Deck(testCardsChance);
		
		// System.out.print(testDeck.drawCard() + "\n");
		
		Box testBox = Box.getInstance(testDeckCommunityChest, testDeckChance);
		
		// testing that cards can be taken out from both decks and that they are in the correct order
		
		assertEquals(testBox.getChanceDeck().getTopCard(), cardChance1, "Card order wrong");
		assertEquals(testBox.getChanceDeck().getTopCard(), cardChance2, "Card Replacement not working");
		assertEquals(testBox.getChanceDeck().getTopCard(), cardChance3, "Card Replacement not working");
		assertEquals(testBox.getChanceDeck().getTopCard(), cardChance4, "Card Replacement not working");
		assertEquals(testBox.getChanceDeck().getTopCard(), cardChance5, "Card Replacement not working");
		
		assertEquals(testBox.getCommunityChestDeck().getTopCard(), card1, "Card order wrong");
		assertEquals(testBox.getCommunityChestDeck().getTopCard(), card2, "Card Replacement not working");
		assertEquals(testBox.getCommunityChestDeck().getTopCard(), card3, "Card Replacement not working");
		assertEquals(testBox.getCommunityChestDeck().getTopCard(), card4, "Card Replacement not working");
		assertEquals(testBox.getCommunityChestDeck().getTopCard(), card5, "Card Replacement not working");
		
		//assertEquals(testBox.getChanceDeck())
		//assertTrue(pairOfDice.getFirstDie() < 7 && pairOfDice.getFirstDie() > 0, "Dice roll 1 out of range");
	}

	@Test
	void testGetHousesAndHotels() {
		Box box = Box.getInstance(null, null);
		assertEquals(12, box.getHotels());
		assertEquals(32, box.getHouses());
		for(int i = 0; i < 32; i++) {
			if(i < 12) {
				box.takeOutHotel();
			}
			box.takeOutHouse();
		}
		assertTrue(box.getHouseShortage());
		assertTrue(box.getHotelShortage());
	}

}
