package gametests;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.jupiter.api.Test;

import game.*;
import setup.GameboardCreator;
import tiles.CardTile;
import tiles.Railroad;
import tiles.Street;
import tiles.Tile;
import tiles.Utilities;

class GameboardTest {

	@Test
	void testGameboardCreation() {
		String filename = "CustomGameboardTest.txt";
		Gameboard gb = GameboardCreator.createGameboard(filename);
		
		assertTrue(gb.get(0) instanceof CardTile);
		assertTrue(gb.get(11) instanceof Utilities);
		assertTrue(gb.get(9) instanceof Street);
		
		Street ashtonPark = (Street)gb.get(9);
		
		assertTrue(ashtonPark.getColour().equals("Red"));
		assertTrue(ashtonPark.getName().equals("Ashton Park"));
		assertEquals(ashtonPark.getHousePrice(), 50);
	}
	
	@Test
	public void testSort() {
		ArrayList<Tile> testTiles = new ArrayList<Tile>();
		int[] rentHouses = {100, 200, 300 ,400, 500};
		Street washAv = new Street("Washington Avenue", 0, 200, "Red", 100, rentHouses, 100, 200);
		Street connAv = new Street("Connecticut Avenue", 2, 300, "Red", 100, rentHouses, 100, 200);
		Street massAv = new Street("Massachussetts Avenue", 4, 400, "Blue", 100, rentHouses, 100, 200);
		Railroad readRail = new Railroad("Reading Railroad", 3, 240, 25);
		Utilities waterWorks = new Utilities("Water Works", 1, 350, 50);
		testTiles.add(washAv);
		testTiles.add(connAv);
		testTiles.add(massAv);
		testTiles.add(readRail);
		testTiles.add(waterWorks);
		System.out.println("Init done");
		Collections.sort(testTiles);
		assertEquals("Washington Avenue", testTiles.get(0).getName());
		assertEquals("Water Works", testTiles.get(1).getName());
		assertEquals("Connecticut Avenue", testTiles.get(2).getName());
		assertEquals("Reading Railroad", testTiles.get(3).getName());
		assertEquals("Massachussetts Avenue", testTiles.get(4).getName());

	}


}
