package gametests;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

import cards.Card;
import game.Deck;

class DeckTest {

	@Test
	void testDeck() {
		ArrayList<Card> testCards = new ArrayList<Card>();
		Card card1 = new Card("One");
		Card card2 = new Card("Two");
		Card card3 = new Card("Three");
		Card card4 = new Card("Four");
		Card card5 = new Card("Five");
		
		testCards.add(card1);
		testCards.add(card2);
		testCards.add(card3);
		testCards.add(card4);
		testCards.add(card5);
		
		Deck testDeck = new Deck(testCards);
		
		assertEquals(testDeck.getTopCard(), card1, "Card order wrong");
		assertEquals(testDeck.getTopCard(), card2, "Card Replacement not working");
		assertEquals(testDeck.getTopCard(), card3, "Card Replacement not working");
		assertEquals(testDeck.getTopCard(), card4, "Card Replacement not working");
		assertEquals(testDeck.getTopCard(), card5, "Card Replacement not working");
		
		//make sure the deck is shuffled
		for(int i = 0; i < 10; i++) {
			System.out.println(testDeck.getTopCard());
		}
	}

}
