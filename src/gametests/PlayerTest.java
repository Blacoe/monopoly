package gametests;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import cards.Card;
import game.Box;
import game.Deck;
import game.Game;
import game.Player;
import game.Token;
import tiles.Property;
import tiles.Railroad;
import tiles.Street;

class PlayerTest {

	@Test
	void testInitialization() {
		Token token = new Token ("Goose", "Grey");
		Player player1 = new Player("Jim", token, 1500);
		assertEquals(0, player1.getPlayerPosition(), "Player is in wrong location");
		assertTrue(player1.getToken().equals(token));
		assertTrue(player1.getName().equals("Jim"));
		assertFalse(player1.getIsBankrupt());
	}
	
	@Test
	void testMovingPlayer() {
		ArrayList<Card> chance = new ArrayList<Card>();
		ArrayList<Card> communityChest = new ArrayList<Card>();
		Deck chanceDeck = new Deck(chance);
		Deck communityChestDeck = new Deck(communityChest);
		Box box = Box.getInstance(chanceDeck, communityChestDeck);
		Player player1 = new Player("Jim", null, 0);
		player1.movePlayer(12, 40, box);
		assertEquals(12, player1.getPlayerPosition(), "Player did not move correctly");
		player1.movePlayer(12, 40, box);
		player1.movePlayer(12, 40, box);
		player1.movePlayer(12, 40, box);
		assertEquals(8, player1.getPlayerPosition(), "Player did not loop the board");
		assertEquals(200, player1.getAccount(), "Player did not collect 200 on Go");
		player1.movePlayer(-1, 40, box);
		assertEquals(7, player1.getPlayerPosition(), "Player did not go backwards");
	}
	
	@Test
	void testCalculatingNetWorth() {
		Player player1 = new Player("Jim", null, 0);
		player1.deposit(100);
		assertEquals(player1.getNetWorth(), 100);
		int[] rentHouses = {200, 300, 400, 500, 1000};
		Property washSt = new Street("Washington Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		player1.addProperty(washSt);
		assertEquals(player1.getNetWorth(), 200);
		Property GeorgeSt = new Street("George's Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		player1.addProperty(GeorgeSt);
		assertEquals(player1.getNetWorth(), 300);
	}
	
	@Test
	void testPlayerSort() {
		Player player1 = new Player("Jim", null, 100);
		Player player2 = new Player("Jim2", null, 50000);
		Player player3 = new Player("Jim3", null, 2);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(player1);
		players.add(player2);
		players.add(player3);
		Game.sortPlayers(players);
		assertTrue(players.get(0).equals(player2));
		assertTrue(players.get(1).equals(player1));
		assertTrue(players.get(2).equals(player3));
	}
	
	@Test
	void testWithdrawAndDeposit() {
		Player player1 = new Player("Jim", null, 100);
		assertEquals(player1.getAccount(), 100, "Starting amount wrong");
		player1.deposit(200);
		assertEquals(player1.getAccount(), 300, "Deposit not working");
		player1.withdraw(34);
		assertEquals(player1.getAccount(), 266, "Withdraw not working");
	}
	
	@Test
	void testCanAfford() {
		Player player1 = new Player("Jim", null, 100);
		assertTrue(player1.canAfford(100));
	}
	
	@Test
	void testCompareTo() {
		Player player1 = new Player("Jim", null, 100);
		Player player2 = new Player("Jane", null, 200);
		assertEquals(player1.compareTo(player2), -1);
		assertEquals(player2.compareTo(player1), 1);
		player1.deposit(100);
		assertEquals(player2.compareTo(player1), 0);
	}
	
	@Test
	void testJailEvents() {
		Player player1 = new Player("Jim", null, 100);
		assertFalse(player1.getIsInJail());
		player1.putInJail();
		assertTrue(player1.getIsInJail());
		player1.giveGetOutOfJailCard();
		assertEquals(player1.getNumGetOutOfJailCards(), 1);
		player1.giveGetOutOfJailCard();
		assertEquals(player1.getNumGetOutOfJailCards(), 2);
		player1.takeGetOutOfJailCard();
		assertEquals(player1.getNumGetOutOfJailCards(), 1);
		player1.useGetOutOfJailCard();
		assertFalse(player1.getIsInJail());
		assertEquals(player1.getNumGetOutOfJailCards(), 0);
		player1.putInJail();
		assertTrue(player1.getIsInJail());
		player1.getOutOfJail();
		assertFalse(player1.getIsInJail());
	}
	
	@Test
	void testAddAndRemoveProperties() {
		Player player1 = new Player("Jim", null, 100);
		Property boardwalk = new Railroad("BoardWalk", 0, 0, 0);
		Street brown = new Street("BoardWalk", 0, 0, "Brown", 0, null, 0, 0);
		assertTrue(player1.getProperties().isEmpty());
		player1.addProperty(boardwalk);
		assertTrue(player1.getProperties().get(0).equals(boardwalk));
		player1.removeProperty(boardwalk);
		assertTrue(player1.getProperties().isEmpty());
		player1.addProperty(brown);
	}
	
	@Test
	void testRemoveStreet() {
		Player player1 = new Player("Jim", null, 10000);
		Box box = Box.getInstance(null, null);
		int[] rentHouses = {200, 300, 400, 500, 1000};
		Street washSt = new Street("Washington Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Street brianSt = new Street("Brian Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Street broadSt = new Street("Broad Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		player1.addProperty(washSt);
		player1.addProperty(brianSt);		
		player1.addProperty(broadSt);
		
		player1.buyHouse(washSt, box);
		assertFalse(player1.removeProperty(washSt));
		player1.sellHouse(washSt, box);
		assertTrue(player1.removeProperty(washSt));
	}
	
	@Test
	void testJumpPlayer() {
		Box box = Box.getInstance(null, null);
		Player player1 = new Player("Jim", null, 100);
		assertEquals(player1.getPlayerPosition(), 0);
		player1.jumpPlayer(13, true, box);
		assertEquals(player1.getPlayerPosition(), 13);
		assertEquals(player1.getAccount(), 100);
		player1.jumpPlayer(6, true, box);
		assertEquals(player1.getPlayerPosition(), 6);
		assertEquals(player1.getAccount(), 300);
		player1.jumpPlayer(5, false, box);
		assertEquals(player1.getPlayerPosition(), 5);
		assertEquals(player1.getAccount(), 300);
	}
	
	@Test
	void testSellPropertyToBank() {
		Player player1 = new Player("Jim", null, 100);
		Property boardwalk = new Railroad("BoardWalk", 0, 100, 0);
		player1.addProperty(boardwalk);
		assertTrue(player1.getProperties().get(0).equals(boardwalk));
		player1.sellPropertyToBank(boardwalk);
		assertEquals(150, player1.getAccount());
		assertTrue(player1.getProperties().isEmpty());
	}
	
	@Test
	void testGetHousesOfColour() {
		Player player1 = new Player("Jim", null, 10000);
		Box box = Box.getInstance(null, null);
		int[] rentHouses = {200, 300, 400, 500, 1000};
		Street washSt = new Street("Washington Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Street brianSt = new Street("Brian Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Street broadSt = new Street("Broad Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		player1.addProperty(washSt);
		player1.addProperty(brianSt);
		
		assertFalse(washSt.getCanBuildHouse());
		assertFalse(washSt.getDoubleRent());
		
		assertTrue(player1.findHousesOfColour("Red").get(0).equals(washSt));
		assertTrue(player1.findHousesOfColour("Red").get(1).equals(brianSt));
		
		player1.addProperty(broadSt);
		assertTrue(washSt.getCanBuildHouse());
		assertTrue(washSt.getDoubleRent());
		
		assertTrue(player1.buyHouse(washSt, box));
		assertFalse(player1.buyHouse(washSt, box));
		
		assertTrue(brianSt.getDoubleRent());
		assertTrue(player1.buyHouse(brianSt, box));
		assertFalse(player1.buyHouse(brianSt, box));
		
		assertTrue(player1.buyHouse(broadSt, box));
		assertFalse(broadSt.getDoubleRent());
		assertFalse(player1.buyHotel(broadSt, box));
		
		assertTrue(player1.buyHouse(broadSt, box));
		assertFalse(player1.buyHouse(broadSt, box));
		assertTrue(player1.buyHouse(brianSt, box));
		assertTrue(player1.buyHouse(washSt, box));
		
		assertTrue(player1.buyHouse(broadSt, box));
		assertTrue(player1.buyHouse(brianSt, box));
		assertTrue(player1.buyHouse(washSt, box));
		assertTrue(player1.buyHouse(broadSt, box));
		assertTrue(player1.buyHouse(brianSt, box));
		assertTrue(player1.buyHouse(washSt, box));
		
		assertFalse(player1.buyHouse(broadSt, box));
		assertFalse(player1.buyHouse(brianSt, box));
		assertFalse(player1.buyHouse(washSt, box));
		
		assertTrue(player1.buyHotel(broadSt, box));
		assertFalse(player1.buyHotel(broadSt, box));
		assertTrue(player1.buyHotel(brianSt, box));
		assertTrue(player1.buyHotel(washSt, box));
	}
	
	@Test
	void testSellingHouses() {
		Player player1 = new Player("Jim", null, 10000);
		Box box = Box.getInstance(null, null);
		int[] rentHouses = {200, 300, 400, 500, 1000};
		Street washSt = new Street("Washington Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Street brianSt = new Street("Brian Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		Street broadSt = new Street("Broad Street", 0, 100, "Red", 100, rentHouses, 100, 200);
		player1.addProperty(washSt);
		player1.addProperty(brianSt);		
		player1.addProperty(broadSt);
		
		player1.buyHouse(washSt, box);
		player1.buyHouse(brianSt, box);
		player1.buyHouse(broadSt, box);
		
		player1.buyHouse(washSt, box);
		player1.buyHouse(brianSt, box);
		player1.buyHouse(broadSt, box);
		
		player1.buyHouse(washSt, box);
		player1.buyHouse(brianSt, box);
		player1.buyHouse(broadSt, box);
		
		player1.buyHouse(washSt, box);
		player1.buyHouse(brianSt, box);
		player1.buyHouse(broadSt, box);
		
		player1.buyHotel(washSt, box);
		player1.buyHotel(brianSt, box);
		player1.buyHotel(broadSt, box);
		
		assertTrue(player1.sellHotel(washSt, box));
		assertFalse(player1.sellHotel(washSt, box));
		assertTrue(player1.sellHotel(brianSt, box));
		assertTrue(player1.sellHotel(broadSt, box));
		
		assertTrue(player1.sellHouse(washSt, box));
		assertFalse(player1.sellHouse(washSt, box));
		assertTrue(player1.sellHouse(brianSt, box));
		assertFalse(player1.sellHouse(brianSt, box));
		assertTrue(player1.sellHouse(broadSt, box));
		
		assertTrue(player1.sellHouse(washSt, box));
		assertFalse(player1.sellHouse(washSt, box));
		assertTrue(player1.sellHouse(brianSt, box));
		assertFalse(player1.sellHouse(brianSt, box));
		assertTrue(player1.sellHouse(broadSt, box));
		
		assertTrue(player1.sellHouse(washSt, box));
		assertFalse(player1.sellHouse(washSt, box));
		assertTrue(player1.sellHouse(brianSt, box));
		assertFalse(player1.sellHouse(brianSt, box));
		assertTrue(player1.sellHouse(broadSt, box));
		
		assertTrue(player1.sellHouse(washSt, box));
		assertFalse(player1.sellHouse(washSt, box));
		assertTrue(player1.sellHouse(brianSt, box));
		assertFalse(player1.sellHouse(brianSt, box));
		assertTrue(player1.sellHouse(broadSt, box));
		
		assertFalse(player1.sellHouse(washSt, box));
	}
	
}
