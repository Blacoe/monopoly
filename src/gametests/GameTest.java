package gametests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import game.Game;
import game.Player;

class GameTest {

	@Test
	void testHasWon() {
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(new Player("Jim", null, 100));
		players.add(new Player("Jen", null, 0));
		players.add(new Player("Jeff", null, 100));
		players.add(new Player("Jemimah", null, 0));
		assertFalse(Game.hasWon(players));
		players.get(0).setIsBankrupt(true);
		players.get(1).setIsBankrupt(true);
		assertTrue(Game.hasWon(players));
	}
	

}
