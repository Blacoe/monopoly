package gametests;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import game.Dice;

class DiceTest {

	@Test
	void testDiceRoll() {
		Dice pairOfDice = Dice.getInstance();
		pairOfDice.roll();
		assertEquals(pairOfDice.getDiceRoll(), pairOfDice.getFirstDie() + pairOfDice.getSecondDie(), "Dice total not correct");
		assertTrue(pairOfDice.getFirstDie() < 7 && pairOfDice.getFirstDie() > 0, "Dice roll 1 out of range");
		assertTrue(pairOfDice.getSecondDie() < 7 && pairOfDice.getSecondDie() > 0, "Dice roll 2 out of range");
	}
	
	@Test
	void testIsDoubles() {
		Dice pairOfDice = Dice.getInstance();
		pairOfDice.roll();
		assertEquals(pairOfDice.isDoubles(), pairOfDice.getFirstDie() == pairOfDice.getSecondDie(), "Doubles detected incorrectly");
	}

}
