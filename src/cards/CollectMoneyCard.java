package cards;

import java.util.ArrayList;

import game.*;

// This class extends the Card class. It allows the player passed into it to collect a specified amount of money.
// (Specified upon initialization of card.)

public class CollectMoneyCard extends Card{
	private int moneyAmount;

	public CollectMoneyCard(String text, int moneyAmount) {
		super(text);
		this.moneyAmount = moneyAmount;
	}
	
	// The interact function adds the amount specified to the user's account. 
	
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		p.deposit(moneyAmount);
	}

}
