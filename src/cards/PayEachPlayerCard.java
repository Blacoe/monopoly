package cards;

import java.util.ArrayList;

import game.*;

// This class extends the card class and provides functionality for one player to pay a specified amount to each player 
// (deducting it from the player's account). The amount of money to be deposited to each player is specified upon
// initialization of the card. 

public class PayEachPlayerCard extends Card{
	private int moneyAmount;

	public PayEachPlayerCard(String text, int moneyAmount) {
		super(text);
		this.moneyAmount = moneyAmount;
	}
	
	// The interact method over rides the original method from the Card class being extended. This method contains
	// logic that checks if the player has enough money in their account to deposit to each player, and deposits 
	// the specified amount of money to each player upon each iteration. If the player does not have enough money 
	// the bankruptcyAction() method is called. 
	
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		int totalCost = moneyAmount * (players.size()-1);
		if(p.canAfford(totalCost)) {
			p.withdraw(totalCost);
			for(int i = 0; i<players.size(); i++) {
				if(!players.get(i).equals(p)) {
					players.get(i).deposit(moneyAmount);
				}
			}
		}
		else {
			p.bankruptcyAction(true, moneyAmount, null, box, players);
		}
	}
}
