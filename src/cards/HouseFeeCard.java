package cards;

import java.util.ArrayList;

import game.*;
import tiles.Street;

// This class extends the Card class. It makes a player pay a set amount for each house and hotel they own. This set amount 
// is specified upon initialization of the object.

public class HouseFeeCard extends Card{
	int houseFee;
	int hotelFee;

	public HouseFeeCard(String text, int houseFee, int hotelFee) {
		super(text);
		this.houseFee = houseFee;
		this.hotelFee = hotelFee;
	}
	
	// The interact method overrides the same method from the Card class. It provides the logic to calculate the total
	// amount of money due from the passed through player by taking in their hotel and house count and having the amount
	// due from each one. It then deducts this amount from that players account. 
	
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		int houseCount = 0;
		int hotelCount = 0;
		for(int i = 0; i<p.getProperties().size(); i++) {
			if(p.getProperties().get(i) instanceof Street) {
				houseCount = houseCount + ((Street)p.getProperties().get(i)).getNumHouses();
				hotelCount = hotelCount + ((Street)p.getProperties().get(i)).getNumHotels();
			}
		}
		int totalFee = (houseFee * houseCount) + (hotelFee * hotelCount);
		
		if(p.canAfford(totalFee)) {
			p.withdraw(totalFee);
		}
		else {
			
		}
	}

}
