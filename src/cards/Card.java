package cards;

import java.util.ArrayList;

import game.*;

// This class is used as a superclass for all the other card classes, allowing a title and text to be assigned to each card. 

public class Card implements Interactable{
	protected String text;
	protected String title;
	
	public Card(String text) {
		this.text = text;
	}
	
	public String getText() {
		return this.text;
	}
	
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		//to be overwritten by subclasses
	}
}
