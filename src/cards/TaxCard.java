package cards;

import java.util.ArrayList;

import game.*;

// This class extends the Card class. It deducts a specified 'tax' amount from a player. The tax amount is specified upon 
// initialization of the TaxCard object. 

public class TaxCard extends Card{
	private int tax;

	public TaxCard(String text, int tax) {
		super(text);
		this.tax = tax;
	}
	
	// The interact method overrides the method set in the Card class. It checks if the passed through player can afford the 
	// tax then deducts it from their account, if they can't then it executes the bankruptyAction() method on that player. 
	
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		if(p.canAfford(tax)) {
			p.withdraw(tax);
		}
		else {
			p.bankruptcyAction(true, tax, null, box, players);
		}
	}

}
