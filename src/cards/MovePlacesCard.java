package cards;

import java.util.ArrayList;

import game.*;

//This class extends the Card class. This card makes the player move a specified number of tiles.The number of
// spaces is specified upon initialization of the card object.

public class MovePlacesCard extends Card{
	int numSpaces;

	public MovePlacesCard(String text, int numSpaces) {
		super(text);
		this.numSpaces = numSpaces;
	}

	// The interact method overrides the original method from the Card class. It uses the movePlayer() method from the 
	// Player class to change the position of the player passed through to the interact method. That tile is then activated
	// using the interact method contained in the Tile class.	
	
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		p.movePlayer(numSpaces, gameboard.size(), box);
		gameboard.get(p.getPlayerPosition()).interact(p, players, box, gameboard, dice);
	}
}
