package cards;

import java.util.ArrayList;

import game.*;

// This class extends the Card class. This card makes the player go to the specified destination, *and* pass Go
// if it is on route, collecting $ 200. The position is specified upon initialization of the card object.

public class GoToCard extends Card{
	private int destinationPosition;

	public GoToCard(String text, int destinationPosition) {
		super(text);
		this.destinationPosition = destinationPosition;
	}
	
	// The interact method overrides the original method from the Card class. It uses the jumpPlayer() method from the 
	// Player class to change the position of the player passed through to the interact method. That tile is then activated
	// using the interact method contained in the Tile class.
	
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		p.jumpPlayer(destinationPosition, true, box); // The true boolean here allows the player to collect 200.
		gameboard.get(p.getPlayerPosition()).interact(p, players, box, gameboard, dice);
	}

}
