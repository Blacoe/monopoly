package cards;

import java.util.ArrayList;

import game.*;
import tiles.Railroad;
import tiles.Utilities;

//This class extends card and provides functionality for a player to advance to the nearest utility or railroad. 

public class AdvanceToNearestCard extends Card{
	private String type;

	public AdvanceToNearestCard(String text, String type) {
		super(text);
		this.type = type;
	}

	// This interact method takes in the player that receives the AdvanceToNearestCard. It moves the player to the nearest
	// Utility or Railroad depending on the type of the object when it is initialized in the game. It also includes functionality
	// to deduct 10 times the player dice roll if it is a utility, or 2 times the rent of the railroad if it is a railroad. 
	
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		if(type.equals("Utilities")) {
			while(!(gameboard.get(p.getPlayerPosition()) instanceof Utilities)) {
				p.movePlayer(1, gameboard.size(),box);
			}
			if(((Utilities)gameboard.get(p.getPlayerPosition())).getIsOwned()) {
				dice.roll();
				int rent = dice.getDiceRoll() * 10;
				
				if(p.canAfford(rent)) {
					p.withdraw(rent);
					((Utilities)gameboard.get(p.getPlayerPosition())).getOwner().deposit(rent);
				}
				else {
					p.bankruptcyAction(false, ((Utilities)gameboard.get(p.getPlayerPosition())).calculateRent(dice), ((Utilities)gameboard.get(p.getPlayerPosition())).getOwner(), box, players);
				}

			}
			else {
				((Utilities)gameboard.get(p.getPlayerPosition())).interact(p, players, box, gameboard, dice);
			}
		}
		
		if(type.equals("Railroad")) {
			while(!(gameboard.get(p.getPlayerPosition()) instanceof Railroad)) {
				p.movePlayer(1, gameboard.size(), box);
			}
			if(((Railroad)gameboard.get(p.getPlayerPosition())).getIsOwned()) {
				int rent = 2 * ((Railroad)gameboard.get(p.getPlayerPosition())).calculateRent(dice);
				
				if(p.canAfford(rent)) {
					p.withdraw(rent);
					((Railroad)gameboard.get(p.getPlayerPosition())).getOwner().deposit(rent);
				}
				else {
					p.bankruptcyAction(false, ((Railroad)gameboard.get(p.getPlayerPosition())).calculateRent(dice), ((Railroad)gameboard.get(p.getPlayerPosition())).getOwner(), box, players);
				}
			}
			else {
				((Railroad)gameboard.get(p.getPlayerPosition())).interact(p, players, box, gameboard, dice);
			}
		}
	}
	
}
