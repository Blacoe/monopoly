package cards;

import java.util.ArrayList;

import game.*;

// This class extends card and provides functionality for one player to gain a specified amount from each player 
// (deducting it from their account). The amount of money to be extracted from each player is specified upon
// initialization of the card. 

public class CollectFromEachPlayerCard extends Card{
	private int moneyAmount;

	public CollectFromEachPlayerCard(String text, int moneyAmount) {
		super(text);
		this.moneyAmount = moneyAmount;
	}

	// The interact method over rides the original method from the Card class being extended. This method contains
	// logic that goes through each player and checks if they have the necessary amount of money, and if they do it
	// deducts it from their account. This amount is then deposited into the account of the player passed in. 
	
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		for(int i = 0; i<players.size(); i++) {
			if(!players.get(i).equals(p)) {
				if(players.get(i).canAfford(moneyAmount)) {
					players.get(i).withdraw(moneyAmount);
					p.deposit(moneyAmount);
				}
				else {
					
				}
			}
		}
	}
}
