package cards;

import java.util.ArrayList;

import game.*;

// This class extends the Card class. It gives a specified player a get out of jail card. 

public class GetOutOfJailCard extends Card{

	public GetOutOfJailCard(String text) {
		super(text);
	}

	// The interact method overrides the method set in the Card class. It uses the giveGetOutOfJailCard() method
	// to assign a get out of jail card to the specified player when using the interact() method. 
	
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		p.giveGetOutOfJailCard();
	}
}
