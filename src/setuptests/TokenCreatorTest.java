package setuptests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import game.Token;
import setup.TokenCreator;

class TokenCreatorTest {

	@Test
	void testTokenCreator() {
		ArrayList<Token> tokens = TokenCreator.tokenCreator("CustomTokens.txt");
		assertTrue(tokens.get(0).getName().equals("horse"));
	}

}
