package setuptests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cards.AdvanceToNearestCard;
import game.*;
import setup.DeckCreator;

class DeckCreatorTest {

	@Test
	void test() {
		Deck deck = DeckCreator.createDeck("CustomCommunityChestDeck.txt");
		assertTrue(deck.getTopCard() instanceof AdvanceToNearestCard);
	}

}
