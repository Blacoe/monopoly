package setuptests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import game.Gameboard;
import setup.GameboardCreator;

class GameboardCreatorTest {

	@Test
	void testGameboardCreator() {
		Gameboard gb = GameboardCreator.createGameboard("CustomGameboard.txt");
		assertTrue(gb.get(0).getName().equals("GO"));
	}

}
