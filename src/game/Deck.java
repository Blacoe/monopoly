package game;

import java.util.ArrayList;
import java.util.Collections;

import cards.Card;
import menus.TileActionMenus;

public class Deck {
	private ArrayList<Card> cardDeck;
	private int numDraws;
	
	public Deck(ArrayList<Card> cardDeck) {
		this.cardDeck = cardDeck;
		this.numDraws = 0;
	}
	
	//the comparable interface (array list) allows the deck to be shuffled
	public void shuffle() {
		Collections.shuffle(cardDeck);
	}
	
	//only used for testing (has same logic as drawCard with different output)
	public Card getTopCard() {
		if(numDraws >= cardDeck.size()) {
			shuffle();
			numDraws = 0;
		}
		cardDeck.get(0);
		Card temp = cardDeck.get(0);
		this.numDraws++;
		cardDeck.remove(0);
		cardDeck.add(temp);
		return temp;
	}
	
	//this takes the top card, places it at the bottom
	//if the deck gets emptied, the cards get shuffled
	//each card has an interact function from the interface
	//the player info is passed from the chance/community tile into
	//the card so it can be interacted with
	public void drawCard(Player p, Box box, Gameboard gameboard, ArrayList<Player> players, Dice dice, boolean isChance) {
		if(numDraws >= cardDeck.size()) {
			shuffle();
			numDraws = 0;
		}
		TileActionMenus.cardMenu(cardDeck.get(0), isChance);
		cardDeck.get(0).interact(p,  players, box, gameboard, dice);
		Card temp = cardDeck.get(0);
		this.numDraws++;
		cardDeck.remove(0);
		cardDeck.add(temp);
	}
}
