package game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import menus.InputHandler;
import menus.JailMenus;
import menus.PassiveMenus;
import menus.PlayerMenu;
import menus.SetupMenus;
import setup.DeckCreator;
import setup.GameboardCreator;
import setup.TokenCreator;

public class Game {
	
	static int i = 0;
	static int playerTotal = 1;
	
	public static void main(String args[]) throws IOException {
		
		ArrayList<Token> tokens = TokenCreator.tokenCreator("CustomTokens.txt"); 				    	// Initializing array of chance cards
		
		// initialise the gameboard using GameboardCreator class 
		Gameboard gameboard = GameboardCreator.createGameboard("CustomGameboard.txt");

		//use DeckCreator to read txt files and create the two decks of cards
		Deck communityChestDeck = DeckCreator.createDeck("OfficialCommunityChest.txt");
		Deck chanceDeck = DeckCreator.createDeck("OfficialChance.txt");
		
		communityChestDeck.shuffle();
		chanceDeck.shuffle();
		
		Box box = Box.getInstance(chanceDeck, communityChestDeck);					// Initializing Box object with both above Deck objects
		
		// get player names and number of players
		playerTotal = SetupMenus.choosePlayers();								// Bring up menu to choose how many players are playing
		
		ArrayList<Player> players = new ArrayList<Player>(); 					// Create an array list to hold the player instances
		
		// Ask what the starting amount of money will be
		int startingMoney = SetupMenus.chooseStartingMoney();
		
		// Create an instance of player using input from user to create parameters for name string and token object type.

		for(int i = 0; i < playerTotal; i++) {
			String playerName = SetupMenus.createPlayer(i+1);
			Token playerToken = SetupMenus.createPlayerTokens(playerName, tokens);
			players.add(new Player(playerName, playerToken, startingMoney));
		}
		
		ArrayList<Player> positionList = new ArrayList<Player>(players);
		
		while(!hasWon(positionList)) { // this while loop keeps the game iterating until a win condition has been met. 
			Dice pairOfDice = Dice.getInstance();
			for(i = 0; i < players.size(); i++) {       									// this for loop iterates through each player in the game
				boolean go = !(players.get(i).getIsInJail());	                        // boolean that flags whether or not player can take a second or third turn
				if(!go) {
					//We display the jail menu here, and from it we get the player's choice
					//their choice influences the next action: roll, pay fine...
					int jailChoice = JailMenus.inJailMenu(players.get(i), players, box);
					//choice 3 is use a get out of jail card
					if(jailChoice == 3) {
						if(players.get(i).useGetOutOfJailCard()) {
							System.out.println("Take off those shackles "+players.get(i).getName()+", because you're free from jail!");
							go = false;
						}
						else {
							System.out.println("You don't have one!");
						}
					}
					//choice 2 is to pay the fine
					else if(jailChoice == 2) {
						players.get(i).withdraw(50);
						players.get(i).getOutOfJail();
						System.out.println("FREEDOM!");
						go = false;
					}
					//choice 4 is to buy a card
					else if(jailChoice == 4) {
						if(JailMenus.seekJailCard(players.get(i), players)) {
							System.out.println("Take off those shackles "+players.get(i).getName()+", because you're free from jail!");
							players.get(i).getOutOfJail();
							go = false;
						}
						else {
							System.out.println("Sorry, not today");
						}
					}
					//otherwise try for doubles
					else {
						pairOfDice.roll();
						if(pairOfDice.isDoubles()) {
							//Out of Jail Menu
							pairOfDice.printRoll();
							System.out.println("Take off those shackles "+players.get(i).getName()+", because you're free from jail!");
							players.get(i).getOutOfJail();
							go = false;
						}
						else {
							pairOfDice.printRoll();
							System.out.println("Sorry "+players.get(i).getName()+", no doubles means you are still in Jail.");
						}
					}
				}
				int rolls = 0;															// integer that holds the number of rolls on a given turn
				int numberOfSpaces;														// integer that holds the number of spaces to be moved
				while(go == true && players.get(i).getIsInJail() == false ) {	        // loop that remains true as long as the player has rolled doubles and is not going to jail
					PlayerMenu.startTurnMenu(players.get(i), rolls, box, players);		// wait for user to hit enter key
					pairOfDice.roll();													// roll the dice
					numberOfSpaces = pairOfDice.getDiceRoll();							// get the integer that is the sum of both die
					rolls++;															// add one to the count of rolls this turn
					pairOfDice.printRoll();												// print to the screen the roll result					
					players.get(i).movePlayer(numberOfSpaces, gameboard.size(), box);	// add number of roll to player position
					System.out.println("You landed on: "+ gameboard.get(players.get(i).getPlayerPosition()).getName());
					gameboard.get(players.get(i).getPlayerPosition()).interact(players.get(i), players, box, gameboard, pairOfDice);
					bankruptcyCheck(players.get(i), players);
					if(pairOfDice.isDoubles()) {
						if(rolls == 3) {
							players.get(i).putInJail();
							players.get(i).jumpPlayer(gameboard.indexOfJail(), false, box);
							go = false;
							System.out.println("You rolled 3 doubles: GO TO JAIL!");
							InputHandler.promptEnterKey();
						}
					}			
					//no doubles means end of go
					else {
						go = false;
					}
				}			
			}
		} //End of game
		
		sortPlayers(positionList);
		PassiveMenus.winners(positionList);

		}
	
	//at the end the players are sorted into winning positions
	public static void sortPlayers(ArrayList<Player> players) {
		Collections.sort(players, Collections.reverseOrder());
	}
	
	//check if anyone is bankrupt as the game goes
	public static void bankruptcyCheck(Player p, ArrayList<Player> players) {
		if(p.getIsBankrupt()) {
			players.remove(p);
		}
	}
	
	//check if anyone has won yet
	public static boolean hasWon(ArrayList<Player> players) {
		int BankruptCount = 0;
		for(i = 0; i < players.size(); i++) {  
			if(players.get(i).getIsBankrupt()){
				BankruptCount = BankruptCount + 1;
			}
		}
		if(players.size() == 2) {
			if(BankruptCount >= 1) {
				return true;
			}
		}
		if(BankruptCount >= 2) {
			return true;
		}
		return false;
	}
}
