package game;
import java.util.Random;

public class Dice {
	private int firstDie;
	private int secondDie;
	//this is singleton design
	private static Dice single_instance = null;
	
	private Dice() {
		firstDie = 1;
		secondDie = 1;
	}
	
	//the singleton method returns the only instance of Dice
	//if no instance exists it creates it
    public static Dice getInstance() 
    { 
        if (single_instance == null) 
            single_instance = new Dice(); 
  
        return single_instance; 
    } 
	
	public void roll() {
		Random rand = new Random();
		this.firstDie = rand.nextInt(6) + 1;
		this.secondDie = rand.nextInt(6) + 1;
	}
	
	public boolean isDoubles() {
		if (this.firstDie == this.secondDie) {
			return true;
		}
		
		return false;
	}
	
	// next 2 getters are only used for testing the other methods
	public int getFirstDie() {
		return firstDie;
	}
	
	public int getSecondDie() {
		return secondDie;
	}
	
	//to read dice total for moving player
	public int getDiceRoll() {
		return firstDie + secondDie;
	}
	
	public void printRoll() {
		System.out.println("You rolled a " + firstDie + " and a " + secondDie);
	}
}
