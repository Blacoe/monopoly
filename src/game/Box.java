package game;

public class Box{
	// keeps track of houses and hotels and decks
	private int houses;
	private int hotels;
	private Deck communityChestDeck; 
	private Deck chanceDeck;
	private boolean houseShortage;
	private boolean hotelShortage;
	private static Box single_instance = null;
	
	private Box(Deck chance, Deck communityChest) { // constructor (private)
		this.houses = 32;
		this.hotels = 12;
		this.communityChestDeck = chance;
		this.chanceDeck = communityChest;
		this.houseShortage = false;
		this.hotelShortage = false;
	}
	
	//use singleton design for the box, there should only be one
    public static Box getInstance(Deck chance, Deck communityChest) 
    { 
        if (single_instance == null) 
            single_instance = new Box(chance, communityChest); 
  
        return single_instance; 
    } 
	
	public boolean getHouseShortage() {
		return houseShortage;
	}
	
	public boolean getHotelShortage() {
		return hotelShortage;
	}

	public int getHouses() {
		return houses;
	}

	public int getHotels() {
		return hotels;
	}
	
	//every time a house is purchased it has to be removed from the box
	public void takeOutHouse() {
		if(houses == 1) {
			houseShortage = true;
			houses = houses-1;
		}
		houses = houses - 1;
	}
	
	//if a house is sold it has to be put back
	public void putBackHouses(int x) {
		houses = houses+x;
		if(houseShortage && x>0) {
			houseShortage = false;
		}
	}
	
	//every time a hotel is sold it has to be removed from the box
	public void takeOutHotel() {
		if(hotels == 1) {
			hotelShortage = true;
			hotels = hotels-1;
		}
		hotels = hotels-1;
	}
	
	//if a hotel is sold it has to be put back
	public void putBackHotels(int x) {
		hotels = hotels+x;
		if(hotelShortage && x>0) {
			hotelShortage = false;
		}
	}

	//the decks are accessed from the box
	//if a card needs to be interacted with, the card needs
	//to be drawn from here
	public Deck getCommunityChestDeck() {
		return communityChestDeck;
	}

	public Deck getChanceDeck() {
		return chanceDeck;
	}
}