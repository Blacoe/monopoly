package game;
import java.util.ArrayList;
import java.util.Collections;

import tiles.Jail;
import tiles.Tile;

public class Gameboard {
	ArrayList<Tile> tileSet;
	
	public Gameboard(ArrayList<Tile> tileSet) {
		this.tileSet = tileSet;
		this.orderTiles();
	}
	
	//get game board (index) is the same as getting the tile set index
	public Tile get(int i) {
		return tileSet.get(i);
	}
	
	public int indexOf(Tile t) {
		return tileSet.indexOf(t);
	}
	
	//need to know where jail is to send people there
	public int indexOfJail() {
		for(int i = 0; i < tileSet.size(); i++) {
			if(tileSet.get(i) instanceof Jail) {
				return i;
			}
		}
		return 0;
	}
	
	public int size() {
		return tileSet.size();
	}
	
	public ArrayList<Tile> getTileSet() {
		return tileSet;
	}
	
	public void orderTiles() {
		Collections.sort(tileSet);
	}
}
