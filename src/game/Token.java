package game;

public class Token {
	private String name;
	private String colour;
	private int assignmentStatus = 0;
	
	public Token(String name, String colour) {
		this.name=name;
		this.colour=colour;
	}
	
	public String getName() {
		return name;
	}
	
	public String getColour() {
		return colour;
	}
	
	public int getAssignmentStatus() {
		return assignmentStatus;
	}
	
	public void setAssignmentStatus(int x) {
        this.assignmentStatus = x;
	}
	
	public String toString() {
		return colour + " " + name;
	}
}
