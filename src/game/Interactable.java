package game;

import java.util.ArrayList;

public interface Interactable {
	//this interface is implemented by every class that can be interacted with by the player
	//all these classes have an interact function that acts as an entry point
	//if player lands on a tile, they interact with it
	//likewise if they draw a card, and so on
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice);
}
