package game;

import java.util.ArrayList;

import menus.AuctionMenus;
import menus.BankruptcyMenus;
import tiles.Property;
import tiles.Street;
import tiles.Utilities;

public class Player implements Comparable<Player> {
	private String name;
	private Token token;
	private int position;
	private boolean isBankrupt;
	private int account;
	private boolean isInJail;
	private int numGetOutOfJailCards;
	private ArrayList<Property> ownedProperty;
	
	public Player(String name, Token token, int startingMoney) {
		this.name = name;
		this.token = token;
		position = 0;
		account = startingMoney;
		isBankrupt = false;
		isInJail = false;
		ownedProperty = new ArrayList<Property>();
		numGetOutOfJailCards = 0;
	}
	
	public boolean getIsBankrupt() {
		return isBankrupt;
	}

	public void setIsBankrupt(boolean b) {
		isBankrupt = b;
	}
	
	public int getNumGetOutOfJailCards() {
		return numGetOutOfJailCards;
	}
	
	public void movePlayer(int numberOfSpaces, int boardSize, Box box) {
		//for moving player backwards
		if(numberOfSpaces < 0) {
			//account for looping board
			if (position + numberOfSpaces < 0){
				position = boardSize + position + numberOfSpaces;
			}
			
			else {
				position = position + numberOfSpaces;
			}
		}
		
		//moving player forwards
		else {
			
			if(position + numberOfSpaces < boardSize) {
				position = position + numberOfSpaces;
			}
			
			else {
				position = position + numberOfSpaces - boardSize;
				//passed go 
				account = account + 200;
			}
		}

	}
	
	//this is used for cards that need to move the player to a specific index
	public void jumpPlayer(int destinationPosition, boolean passGo, Box box) {
		if(passGo && destinationPosition < position) {
			account = account + 200;
		}
		position = destinationPosition;
	}
	
	public int getPlayerPosition() {
		return this.position;
	}
	
	public boolean getIsInJail() {
		return isInJail;
	}
	
	public void getOutOfJail() {
		isInJail = false;
	}
	
	public void putInJail() {
		isInJail = true;
	}
	
	public void deposit(int m) {
		account = account + m;
	}
	
	public void withdraw(int m) {
		account = account - m;
	}
	
	//adding a property
	//need to set as owned and change owner
	//need to check if the player has all properties of same color
	public void addProperty(Property p) {
		ownedProperty.add(p);
		p.setOwner(this);
		p.setIsOwned(true);
		//do a house check
		if(p instanceof Street) {
			String colour = ((Street)p).getColour();
			ArrayList <Street> colourStreet = findHousesOfColour(colour);
			if(colour.equals("Brown") || colour.equals("Blue")) {
				if(colourStreet.size() >= 2) {
					for(int i = 0; i < colourStreet.size(); i++) {
						((Street) colourStreet.get(i)).setCanBuildHouse(true);
					}
				}
			}
			else {
				if(colourStreet.size() >= 3) {
					for(int i = 0; i < colourStreet.size(); i++) {
						((Street) colourStreet.get(i)).setCanBuildHouse(true);
						((Street) colourStreet.get(i)).setDoubleRent(true);
					}  
				}
			}
		}
	}
	
	//remove property
	//can't do it if you have houses/hotels
	public boolean removeProperty(Property p) {
		if(p instanceof Street) {
			if(((Street)p).getNumHouses() > 0 || ((Street)p).getNumHotels() > 0 ) {
				return false;
			}
			else {
				//remove ownership
				//make other streets of same color not buildable
				if(ownedProperty.remove(p)) {
					p.setIsOwned(false);
					p.setOwner(null);
					String colour = ((Street)p).getColour();
					ArrayList <Street> colourStreet = findHousesOfColour(colour);
					for(int i = 0; i < colourStreet.size(); i++) {
						colourStreet.get(i).setCanBuildHouse(false);
						colourStreet.get(i).setCanBuildHotel(false);
						colourStreet.get(i).setDoubleRent(false);
					}
					return true;
				}
				return false;
			}
		}
		if(ownedProperty.remove(p)) {
			p.setIsOwned(false);
			p.setOwner(null);
			return true;
		}
		return false;
	}
	
	//give player 1/2 the price back
	public boolean sellPropertyToBank(Property p) {
		if(this.removeProperty(p)) {
			this.deposit(p.getPrice()/2);
			return true;
		}
		return false;
	}
	
	public ArrayList<Property> getProperties(){
		return ownedProperty;
	}
	
	public boolean canAfford(int m) {
		//player can afford to pay m
		if(account - m >= 0) {
			return true;
		}
		//player unable
		return false;
	}
	
	public int getAccount() {
		return account;
	}
	
	public String getName() {
		return name;
	}
	
	public Token getToken() {
		return token;
	}
	
	//returns a string of all properties
	public String getListOfProperties() {
		String list = "";
		for(int i = 0; i < ownedProperty.size(); i++) {
			if(i == ownedProperty.size() - 1) {
				list = list + ownedProperty.get(i).getName();
			}
			else {
				list = list + ownedProperty.get(i).getName() + ", ";
			}
		}
		return list;
	}
	
	//used for menu so player can see their properties
	public String[] toStringPropertyList() {
		String[] list = new String[ownedProperty.size()];
		for(int i = 0; i < ownedProperty.size(); i++) {
			String name = ownedProperty.get(i).getName();
			String type = "Railroad";
			if(ownedProperty.get(i) instanceof Street) {
				type = "Street";
			}
			if(ownedProperty.get(i) instanceof Utilities) {
				type = "Utilities";
			}
			list[i] = (i+1) + ". " + type + ": " + name;
		}
		return list;
	}

	//used to check conditions for buying houses etc...
	//returns a list of houses of said color
	public ArrayList<Street> findHousesOfColour(String colour) {
		ArrayList<Street> colourProperty = new ArrayList<Street>();
		for(int i = 0; i < ownedProperty.size(); i++) {
			if(ownedProperty.get(i) instanceof Street) {
				if(((Street)ownedProperty.get(i)).getColour().equals(colour)) {
					colourProperty.add((Street)ownedProperty.get(i));
				}
			}
		}
		return colourProperty;
	}
	
	//triggers the buildHouse method in street
	//can't be executed unless the house can be built
	//if it can't, return false
	public boolean buyHouse(Street s, Box box) {
		if(s.getCanBuildHouse() && this.canAfford(s.getHousePrice())) {
			s.buildHouse(this);
			this.withdraw(s.getHousePrice());
			box.takeOutHouse();
			return true;
		}
		else {
			return false;
		}
	}
	
	//triggers the buildHotel method in street
	//can't be executed unless the hotel can be built
	//if it can't, return false
	public boolean buyHotel(Street s, Box box) {
		if(s.getCanBuildHotel() && this.canAfford(s.getHotelPrice())) {
			s.buildHotel(this);
			this.withdraw(s.getHotelPrice());
			box.takeOutHotel();
			return true;
		}
		else {
			return false;
		}
	}

	//triggers sellHouse in Street
	//if it can't be sold, return false
	public boolean sellHouse(Street s, Box box) {
		if(s.sellHouse(this)) {
			this.deposit(s.getHousePrice()/2);
			box.putBackHouses(1);
			return true;
		}
		else {
			return false;
		}
	}
	
	//triggers sellHotel in Street
	//if it can't be sold, return false
	public boolean sellHotel(Street s, Box box) {
		if(s.getNumHotels() > 0) {
			s.sellHotel(this);
			this.deposit(s.getHotelPrice()/2);
			box.putBackHotels(1);
			return true;
		}
		else {
			return false;
		}
	}
	
	public void giveGetOutOfJailCard() {
		numGetOutOfJailCards++;
	}
	
	public void takeGetOutOfJailCard() {
		numGetOutOfJailCards--;
	}
	
	//get player out of jail and remove card
	public boolean useGetOutOfJailCard() {
		if(numGetOutOfJailCards > 0) {
			numGetOutOfJailCards--;
			isInJail = false;
			return true;
		}
		else {
			return false;
		}
	}

	public int getNetWorth() {
		// players net worth is sum of the money in their account, their total property value, 
		
		int houseAndHotelSales = 0;
		for(int i = 0; i < ownedProperty.size(); i++) {
			if(ownedProperty.get(i) instanceof Street) {
				houseAndHotelSales = houseAndHotelSales + (((Street)ownedProperty.get(i)).getTotalHouseAndHotelValue());
			}
		}
		
		int propertyValue = 0;
		for(int i = 0; i < ownedProperty.size(); i++) {
			propertyValue = propertyValue + ownedProperty.get(i).getPrice();
		}
		
		int netWorth = account + propertyValue + houseAndHotelSales;
			
		return netWorth;
	}

	public void bankruptcyAction(boolean toBank, int debt, Player p, Box box, ArrayList<Player> players) {
		if(toBank) { //this player owes money to the bank
			//open player menu with selling options
			if(BankruptcyMenus.bankruptcyActionMenu(this, box, debt, players)) {
				this.withdraw(debt);
				BankruptcyMenus.backFromBankruptcy(this);
			}
			else {
				isBankrupt = true;
				//auction off all properties
				ArrayList<Property> auctionProperties = ownedProperty;
				
				//remove all properties
				for(int i = 0; i < ownedProperty.size(); i++) {
					this.removeProperty(ownedProperty.get(i));
				}
				
				//start auction
				for(int i = 0; i < auctionProperties.size(); i++) {
					AuctionMenus.auctionMenu(players, auctionProperties.get(i), box);
				}
			}
		}
		
		else { //this player owes money to another player
			//sell houses and hotels to bank
			BankruptcyMenus.bankruptcyToOtherPlayerMenu(this, p);
			int houseAndHotelSales = 0;
			for(int i = 0; i < ownedProperty.size(); i++) {
				if(ownedProperty.get(i) instanceof Street) {
					houseAndHotelSales = houseAndHotelSales + (((Street)ownedProperty.get(i)).getTotalHouseAndHotelValue())/2;
					((Street)ownedProperty.get(i)).removeAllHousesAndHotels(box);
				}
			}
			
			//remove all properties and give away
			for(int i = 0; i < ownedProperty.size(); i++) {
				Property temp = ownedProperty.get(i);
				this.removeProperty(temp);
				p.addProperty(temp);
			}
			
			//give all money to player
			p.deposit(houseAndHotelSales + this.getAccount());
			this.withdraw(this.getAccount());
			
			//give get out of jail card
			if(numGetOutOfJailCards > 0) {
				for(int i = 0; i < numGetOutOfJailCards; i++) {
					p.giveGetOutOfJailCard();
				}
				numGetOutOfJailCards = 0;
			}
			
			//make bankrupt
			isBankrupt = true;
		}
	}

	@Override
	public int compareTo(Player p) {
		if (this.getNetWorth() == p.getNetWorth()) {
			return 0;
		}
		if (this.getNetWorth() < p.getNetWorth()) {
			return -1;
		}
		return 1;
	}
}