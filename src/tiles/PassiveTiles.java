package tiles;

public interface PassiveTiles {
	//this interface provides structure to tiles that don't do anything:
	//e.g. free parking, GO! and just visiting jail
	
	//this function prompts a menu describing the tile
	public void throwPassiveMenu();
}
