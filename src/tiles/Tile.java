package tiles;

import java.util.ArrayList;

import game.Box;
import game.Dice;
import game.Gameboard;
import game.Interactable;
import game.Player;

public class Tile implements Comparable<Tile>, Interactable{
	protected String name;
	private int position;
	
	public Tile(String name, int position) {
		this.name = name;
		this.position = position;
	}
	
	public int getPosition() {
		return this.position;
	}
	
	public String getName() {
		return this.name;
	}
	
	//to be overridden by classes
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		//this method acts as an entry point to the various different things that 
		//a player can do when they land on a tile
		//in main, the tile can be interacted with using tile.interact()
		//that will retrieve the tile in question with whichever interact functionality
		//it has, overriding this empty method
	}

	//the compareTo method has to be implemented so that the tiles can be ordered
	//when the game board is first created
	@Override
	public int compareTo(Tile compareTile) {
		return this.position - compareTile.getPosition();
	}
	
	public String[] toStringList() {
		String[] list = {"name"};
		//Edit this
		return list;
	}
}
