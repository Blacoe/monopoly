package tiles;

import java.util.ArrayList;

import game.Box;
import game.Dice;
import game.Player;

public class Street extends Property{
	private String colour;
	private int numHouses;
	private int numHotels;
	private boolean canBuildHouse;
	private boolean canBuildHotel;
	private boolean doubleRent;
	private int[] rentHouses;
	private int housePrice;
	private int hotelPrice;
	
	public Street(String name, int position, int price, String colour, int rent, int[] rentHouses, int housePrice, int hotelPrice) {
		super(name, position, price, rent);
		this.colour = colour;
		numHouses = 0;
		numHotels = 0;
		//0-3 rentHouses are rents of 1-4 Houses, 4 is rent of Hotel
		this.rentHouses = rentHouses;
		this.housePrice = housePrice;
		this.hotelPrice = hotelPrice;
		canBuildHouse = false;
		canBuildHotel = false;
		doubleRent = false;
	}

	//rent is calculated by number of Houses
	//if they can charge double rent, then double
	@Override
	public int calculateRent(Dice dice) {
		if(numHotels > 0) {
			return rentHouses[4];
		}
		else if(numHouses > 0) {
			switch(numHouses) {
			case 1: return rentHouses[0];
			case 2: return rentHouses[1];
			case 3: return rentHouses[2];
			case 4: return rentHouses[3];
			default: return rent;
			}
		}
		else {
			if(doubleRent) {
				return rent*2;
			}
			return rent;
		}
	}
	
	public void setCanBuildHouse(boolean b) {
		canBuildHouse = b;
	}
	
	public boolean getCanBuildHouse() {
		return canBuildHouse;
	}
	
	public void setCanBuildHotel(boolean b) {
		canBuildHotel = b;
	}
	
	public boolean getCanBuildHotel() {
		return canBuildHotel;
	}
	
	public void setDoubleRent(boolean b) {
		doubleRent = b;
	}
	
	public boolean getDoubleRent() {
		return doubleRent;
	}
	
	public String getColour() {
		return colour;
	}

	//Player has checks to ensure that the player can build a house
	//if they can, build away
	//then update the boolean controlling whether they can build
	//if the house was built and now all properties are even,
	//then they can all have a house built on them
	//otherwise they cannot
	public void buildHouse(Player p) {
		numHouses++;
		doubleRent = false;
		ArrayList<Street> streetColour = p.findHousesOfColour(colour);
		streetColour.remove(this);
		boolean allEqual = true;
		for(int i = 0; i < streetColour.size(); i++) {
			if(streetColour.get(i).getNumHouses() < this.numHouses) {
				canBuildHouse = false;
				allEqual = false;
			}
		}		
		if(allEqual) {
			if(numHouses == 4) {
				for(int i = 0; i < streetColour.size(); i++) {
					streetColour.get(i).setCanBuildHouse(false);
					streetColour.get(i).setCanBuildHotel(true);
				}
				this.canBuildHouse = false;
				this.canBuildHotel = true;
			}
			else {
				for(int i = 0; i < streetColour.size(); i++) {
					streetColour.get(i).setCanBuildHouse(true);
				}
				this.canBuildHouse = true;
			}
		}
	}
	
	//if player builds a hotel, they can't build another
	public void buildHotel(Player p) {
		numHotels++;
		numHouses = 0;
		canBuildHotel = false;
	}
	
	//this returns a boolean indicating whether or not the player can sell the house
	//the player can sell the house if the houses are evenly spread or,
	//if the property in question has more houses than the others
	public boolean sellHouse(Player p) {
		if(numHouses == 0) {
			return false;
		}
		ArrayList<Street> streetColour = p.findHousesOfColour(colour);
		streetColour.remove(this);
		boolean allEqual = true;
		for(int i = 0; i < streetColour.size(); i++) {
			if(streetColour.get(i).getNumHouses() < this.numHouses) {
				canBuildHouse = false;
			}
		}
		for(int i = 0; i < streetColour.size(); i++) {
			if(streetColour.get(i).getNumHouses() != this.numHouses) {
				allEqual = false;
			}
		}
		if(canBuildHouse && allEqual) {
			numHouses--;
			for(int i = 0; i < streetColour.size(); i++) {
				streetColour.get(i).setCanBuildHouse(false);
				streetColour.get(i).setCanBuildHotel(false);
			}
			if(numHouses == 0) {
				doubleRent = true;
			}
			canBuildHouse = true;
			canBuildHotel = false;
			return true;
		}
		if(canBuildHotel && allEqual) {
			numHouses--;
			for(int i = 0; i < streetColour.size(); i++) {
				streetColour.get(i).setCanBuildHouse(false);
				streetColour.get(i).setCanBuildHotel(false);
			}
			if(numHouses == 0) {
				doubleRent = true;
			}
			canBuildHouse = true;
			canBuildHotel = false;
			return true;
		}
		if((!canBuildHouse) && (numHotels == 0)) {
			numHouses--;
			canBuildHouse = true;
			if(numHouses == 0) {
				doubleRent = true;
			}
			return true;
		}
		return false;
	}
	
	//if the player has a hotel they can sell it
	// they get back their houses and can buy another hotel
	public void sellHotel(Player p) {
		numHotels--;
		numHouses = 4;
		canBuildHotel = true;
	}
	
	public int getNumHouses() {
		return numHouses;
	}
	
	public int getNumHotels() {
		return numHotels;
	}
	
	public int getHousePrice() {
		return housePrice;
	}
	
	public int getHotelPrice() {
		return hotelPrice;
	}
	
	public int getTotalHouseAndHotelValue() {
		return (numHouses*housePrice) + (numHotels*hotelPrice);
	}
	
	//this is used for bankruptcy purposes
	public void removeAllHousesAndHotels(Box box) {
		box.putBackHouses(numHouses);
		if(numHotels > 0) {
			box.putBackHouses(4);
			box.putBackHotels(1);
		}
		numHouses = 0;
		numHotels = 0;
		canBuildHouse = false;
		canBuildHotel = false;
	}
	
	//this overrides Property's method and is displayed in the
	//property menu
	//the list is dynamic, and reflects the current state of the
	//property
	@Override
	public String[] toStringList() {
		String buildHouse = "No";
		String buildHotel = "No";
		if(canBuildHouse) {
			buildHouse = "Yes";
		}
		if(canBuildHotel) {
			buildHotel = "Yes";
		}
		String[] list = new String[7];
		list[0] = name;
		list[1] = "Price: " + price;
		list[2] = "Rent: " + calculateRent(null);
		list[3] = "Colour: " + colour;
		list[4] = "Number of Houses/Hotels: " + numHouses + "/" + numHotels;
		list[5] = "Can Build Houses/Hotels?: " + buildHouse + "/" + buildHotel;
		list[6] = "Price of House/Hotel: " + housePrice + "/" + hotelPrice;
		return list;
	}
}
