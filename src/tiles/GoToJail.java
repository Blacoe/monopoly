package tiles;

import java.util.ArrayList;

import game.Box;
import game.Dice;
import game.Gameboard;
import game.Player;
import menus.PassiveMenus;

public class GoToJail extends Tile{

	public GoToJail(String name, int position) {
		super(name, position);
	}

	//interact puts the user in jail
	//does this by jumping the player to jail and changing their injail boolean
	//menu displays a message
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		p.putInJail();
		p.jumpPlayer(gameboard.indexOfJail(), false, box);
		PassiveMenus.goToJailMenu();
	}
	
}
