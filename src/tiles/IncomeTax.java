package tiles;

import java.util.ArrayList;

import game.Box;
import game.Dice;
import game.Gameboard;
import game.Player;
import menus.PassiveMenus;

public class IncomeTax extends Tile{
	private int tax;

	public IncomeTax(String name, int position, int tax) {
		super(name, position);
		this.tax = tax;
	}
	
	//income tax subtracts money if player can afford
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		PassiveMenus.IncomeTax(p, tax);
		if(p.canAfford(tax)) {
			p.withdraw(tax);
		}
		//if player can't afford, they go to the bankruptcy menu with "tobank" set to true
		else {
			p.bankruptcyAction(true, tax, null, box, players);
		}
	}

}
