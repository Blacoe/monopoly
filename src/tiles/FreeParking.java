package tiles;

import java.util.ArrayList;

import game.Box;
import game.Dice;
import game.Gameboard;
import game.Player;
import menus.PassiveMenus;

public class FreeParking extends Tile implements PassiveTiles{

	public FreeParking(String name, int position) {
		super(name, position);
	}

	//inherited from interface, shows the free parking message
	public void throwPassiveMenu() {
		PassiveMenus.freeParking();
	}
	
	//the interact here just displays the menu
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		throwPassiveMenu();
	}
	
}
