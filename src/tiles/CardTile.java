package tiles;

import java.util.ArrayList;

import game.Box;
import game.Dice;
import game.Gameboard;
import game.Player;

public class CardTile extends Tile{

	private boolean isChance;
	//if isChance, the tile is for chance, else it is community chest 
	//this boolean is set on construction of tile and no getters/setters
	
	public CardTile(String name, int position, boolean isChance) {
		super(name, position);
		this.isChance = isChance;
	}
	
	//the interact function triggers the chance/community deck
	//by drawing the card the player interacts with the card
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		if(isChance) {
			box.getChanceDeck().drawCard(p, box, gameboard, players, dice, isChance);
		}
		else {
			box.getCommunityChestDeck().drawCard(p, box, gameboard, players, dice, isChance);
		}
	}

}
