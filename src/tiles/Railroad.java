package tiles;

import game.Dice;

public class Railroad extends Property{

	public Railroad(String name, int position, int price, int rent) {
		super(name, position, price, rent);
	}

	//calculate rent needs to be overridden because of special rules
	//railroads increase in rent as number of owned railroads increases
	@Override
	public int calculateRent(Dice dice) {
		int rentTemp = rent;
		for(int i = 0; i < owner.getProperties().size(); i++) {
			if(owner.getProperties().get(i) instanceof Railroad) {
				if(!owner.getProperties().get(i).equals(this)) {
					rentTemp = rentTemp*2;
				}
			}
		}
		return rentTemp;		
	}
	
	//this is used for the menu to display the information about
	//this property
	@Override
	public String[] toStringList() {
		String[] list = new String[3];
		list[0] = name;
		list[1] = "Price: " + price;
		list[2] = "Rent: " + calculateRent(null);
		return list;
	}
}
