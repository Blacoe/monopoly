package tiles;

import java.util.ArrayList;

import game.Box;
import game.Dice;
import game.Gameboard;
import game.Player;
import menus.PassiveMenus;

public class Jail extends Tile implements PassiveTiles{

	public Jail(String name, int position) {
		super(name, position);

	}

	//inherited from interface, this displays a message saying just visiting
	public void throwPassiveMenu() {
		PassiveMenus.landJailMenu();
	}
	
	//this is a passive menu since the player is just visiting
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		throwPassiveMenu();
	}

}
