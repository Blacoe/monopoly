package tiles;

import java.util.ArrayList;

import game.Box;
import game.Dice;
import game.Gameboard;
import game.Player;
import menus.PassiveMenus;

public class GoTile extends Tile implements PassiveTiles{

	public GoTile(String name, int position) {
		super(name, position);
	}

	//inherited from interface, shows the GO message
	public void throwPassiveMenu() {
		PassiveMenus.goTile();
	}
	
	//interact just displays the passive menu saying GO
	@Override
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		throwPassiveMenu();
	}
}
