package tiles;

import game.Dice;

public class Utilities extends Property{

	public Utilities(String name, int position, int price, int rent) {
		super(name, position, price, rent);
	}

	//This is used by the menu to display information on this property
	@Override
	public String[] toStringList() {
		String[] list = new String[3];
		list[0] = name;
		list[1] = "Price: " + price;
		list[2] = rentToString();
		return list;
	}
	
	//the rent is calculated based on the dice roll
	//if 2 utilities are owned -> 10*dice roll
	//else 4*dice roll
	@Override
	public int calculateRent(Dice dice) {
		int rentTemp = rent;
		int numUtilities = 0;
		for(int i = 0; i < owner.getProperties().size(); i++) {
			if(owner.getProperties().get(i) instanceof Utilities) {
				numUtilities++;
			}
		}
		if(numUtilities == 2) {
			rentTemp = 10*dice.getDiceRoll();
		}
		if(numUtilities == 1) {
			rentTemp = 4*dice.getDiceRoll();
		}
		return rentTemp;		
	}
	
	//the rent string has to be generated to display information
	//since rent is dynamic
	public String rentToString() {
		int numUtilities = 0;
		for(int i = 0; i < owner.getProperties().size(); i++) {
			if(owner.getProperties().get(i) instanceof Railroad) {
				numUtilities++;
			}
		}
		if(numUtilities == 2) {
			return "10 times dice roll";
		}
		if(numUtilities == 1) {
			return "4 times dice roll";
		}
		return "No Rent Information";
	}
}
