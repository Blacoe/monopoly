package tiles;

import java.util.ArrayList;

import game.Box;
import game.Dice;
import game.Gameboard;
import game.Player;
import menus.AuctionMenus;
import menus.PassiveMenus;
import menus.TileActionMenus;

public class Property extends Tile{
	protected int price;
	protected int rent;
	protected boolean isOwned;
	protected Player owner;
	
	public Property(String name, int position, int price, int rent) {
		super(name, position);
		this.price = price;
		isOwned = false;
		this.rent = rent;
	}
	
	//the interact function forks depending on the circumstances
	@Override 
	public void interact(Player p, ArrayList<Player> players, Box box, Gameboard gameboard, Dice dice) {
		//if the property is owned
		if(isOwned) {
			//if the player owns it they're just visiting
			if(owner.equals(p)) {
				PassiveMenus.justVisitingOwnPropertyMenu(p, this);
			}
			//if someone else owns it they have to pay
			else {
				TileActionMenus.payRentActionMenu(p, this, dice);
				//pay rent handles the bankruptcy conditions
				payRent(p, box, players, dice);
			}
		}
		//if the property is not owned
		else {
			//if player can afford it
			if(p.canAfford(price)) {
				//if they choose to buy
				//'true' in menu parameter indicates whether they can afford
				if(TileActionMenus.propertyActionMenu(p, true, this)) {
					buy(p);
				}
				//if they decline
				else {
					auction(players, box);
				}
			}
			//if they can't afford it
			else {
				TileActionMenus.propertyActionMenu(p, false, this);
				auction(players, box);
			}
		}
	}
	
	//buying property, add it to player's list and withdraw price
	public void buy(Player p) {
		p.withdraw(price);
		p.addProperty(this);
	}
	
	//paying rent, withdraw and deposit if they can afford
	public void payRent(Player p, Box box, ArrayList<Player> players, Dice dice) {
		if(p.canAfford(calculateRent(dice))) {
			p.withdraw(calculateRent(dice));
			owner.deposit(calculateRent(dice));
		}
		//if they can't afford, bankrupt
		else {
			p.bankruptcyAction(false, calculateRent(dice), owner, box, players);
		}
	}
	
	//auction opens the auction menu to commence bidding
	public void auction(ArrayList<Player> players, Box box) {
		AuctionMenus.auctionMenu(players, this, box);
	}
	
	public int getPrice() {
		return price;
	}
	
	//this allows subclasses to have different rents
	//by calculating numHouses for example
	public int calculateRent(Dice dice) {
		//let subclasses override this
		return rent;
	}
	
	public Player getOwner() {
		return owner;
	}
	
	public void setOwner(Player p) {
		owner = p;
	}
	
	//this is used to create a list of property information for the menu
	@Override
	public String[] toStringList() {
		String[] list = {"name"};
		//Edit this
		return list;
	}
	
	public boolean getIsOwned() {
		return isOwned;
	}
	
	public void setIsOwned(boolean b) {
		isOwned = b;
	}

}
